<?php get_header() ?>

<section class="ng-cloak" ng-controller="Ganhadores">
	<div class="fundo-5">
		<div class="center-2">
			<img src="<?php echo get_template_directory_uri()?>/img/banner-ganhadores.png">

			<div class="row">
				<div class="col-xs-8">
					<div class="chamada-form">
						<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>    
								<?php the_content(); ?>
							<?php endwhile; ?>
						<?php endif; ?>
						<hr>

					</div>
				</div>
				<div class="col-xs-12">
					<div class="box-1 scroll">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#">Novembro</a></li>
							<li><a href="#">Dezembro</a></li>
							<li><a href="#">Janeiro</a></li>
						</ul>
						<div id="menu1">
							<div class="table-responsive">

								<table class="table table-hover" >
									<thead>
										<tr>
											<th>
												<span>
													Nome da rede
												</span>
											</th>
											<th>
												<span>
													Nome do premiado
												</span>
											</th>
											<th>
												<span>
													Prêmio
												</span>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="ganhador in ganhadores track by $index">
											<td>{{ ganhador.nome_rede }} </td>
											<td>{{ ganhador.nome_premiado }} </td>
											<td>{{ ganhador.premio }} </td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>




	</div>
</section>


<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/controllers/Ganhadores.js"></script>

<?php get_footer() ?>