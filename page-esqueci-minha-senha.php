<?php get_header(); ?>



<section class="ng-cloak" ng-controller="EsqueciMinhaSenha">
	<div class="fundo-5" style="min-height: 300px; margin-top: -10px;padding: 20px">
		
		<div class="center-3" style="margin-top: 20px;width: 100%;max-width: 930px;">
			<div class="box-1 box-2">
				<div class="row">
					<div class="col-xs-12">
						<h3 class="text-center">Esqueci minha senha</h3>
					</div>
					<div class="col-xs-12">
						<form name="formReset" style="max-width: 550px; margin: auto;">
							<div class="form-group">
								<input required ng-model="login" placeholder="Login" type="text" class="form-control" >
							</div>
							<div class="form-group">
								<button ng-click="recuperar()" ng-disabled="formReset.$invalid" class="pull-right btn btn-primary">Recuperar</button>
							</div>
						</form>
					</div>
					<div class="col-xs-12">
						<br>
						<div class="alertas"></div>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<style type="text/css">
form.form-header-login  {
	display: none;
}
</style>

<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/controllers/Esqueci-Minha-Senha.js"></script>

<?php get_footer() ?>