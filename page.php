<?php get_header() ?>

<section class="page-padrao fundo-5" style="min-height: 300px; margin-top: -10px;padding: 20px">

	<div class="container">
		<?php if (have_posts()): while (have_posts()) : the_post(); ?>


			<?php the_content();?>

		<?php  endwhile; endif; ?>
	</div>


</section>


<?php get_footer() ?>