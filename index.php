<?php get_header(); ?>


<section>
	<div class="corpo-home fundo-2">
		<div class="container text-center">
			<div class="col-sm-6">
				<img data-animate-in="fadeInLeft maring-top--90" src="<?php echo get_template_directory_uri()?>/img/home-corpo-banner-1.png" >
			</div>
			<div class="col-sm-6">
				<img data-animate-in="bounceInRight" src="<?php echo get_template_directory_uri()?>/img/home-corpo-banner-2.png">
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-6">
				<img data-animate-in="fadeInLeft" src="<?php echo get_template_directory_uri()?>/img/home-corpo-banner-3.png">
			</div>
			<div class="col-sm-6">
				<img data-animate-in="bounceInRight" class="bicicleta-home" src="<?php echo get_template_directory_uri()?>/img/home-corpo-banner-4.png?v2" >
			</div>
		</div>
	</div>

	<div id="como-participar" class="fundo-3">
		<div class="container text-center">
			<div class="col-sm-12">
				<img data-animate-in="fadeIn" src="<?php echo get_template_directory_uri()?>/img/banner-como-participar.png">
			</div>
			<div class="col-sm-12">
				<h2 data-animate-in="fadeIn" class="text-center jackerton " style="color: #fff; font-size: 50px;">
					QUANTO MAIS CUPONS TIVER, <br> MAIS CHANCES VOCÊ TEM DE GANHAR.
				</h2>
			</div>
		</div>
	</div>

	<div id="premios" class="fundo-2">
		<div class="container text-center">
			<div class="col-sm-12">
				<img data-animate-in="fadeIn" src="<?php echo get_template_directory_uri()?>/img/banner-premio.png">
			</div>
			<div class="col-sm-12">
				<img data-animate-in="fadeIn" src="<?php echo get_template_directory_uri()?>/img/banner-bicicleta.png">
			</div>

		</div>
	</div>
	<div class="fundo-3 padding-bottom-50">
		<div class="container text-center">
			<div class="col-sm-12">
				<img data-animate-in="fadeIn" src="<?php echo get_template_directory_uri()?>/img/banner-participantes.png" style="position: relative;z-index: 1">
			</div>
			<div class="col-sm-12 banner-home-display">
				<img src="<?php echo get_template_directory_uri()?>/img/banner-display.png">
			</div>
			<div class="col-sm-12">
				<h2 data-animate-in="fadeIn" class="text-center jackerton " style="color: #fff; font-size: 45px;">
					VEJA LISTA DE PRODUTOS PARTICIPANTES DA CAMPANHA <a href="<?php echo home_url('/produtos-participantes');?>" class="jackerton" style="color: #fff;text-decoration: underline;">AQUI</a>!
				</h2>
			</div>
		</div>
	</div>

</section>


<script type="text/javascript">

	$(document).ready(function () {
		

		function getCampo(param) {
			var url = new URL(window.location.href);
			return url.searchParams.get(param);
		}

		let campo = getCampo('area')


		animar(campo)

		function animar(campo) {

			if (campo ) {

				var hash = '#'+campo;

				$('html, body').animate({
					scrollTop: $(hash).offset().top
				}, 800);
			}

		}


	})

</script>
<?php get_footer(); ?>