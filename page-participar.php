<?php get_header(); ?>

<script type="text/javascript">
	function getDadosFacebook () {
		FB.api(
			'/me',
			'GET',
			{"fields":"id,name,birthday,email,first_name,last_name,gender,photos{picture},picture{url}"},
			function(response) {
				debugger
				$('[ng-model="participante.email1"]').val( response.email );
				$('[ng-model="participante.dtNasc"]').val( response.birthday );
				$('[ng-model="participante.name"]').val( response.first_name );
				$('[ng-model="participante.sobrenome"]').val( response.last_name );
			});
	}

</script>
<section class="ng-cloak" ng-controller="Participar">
	<div class="fundo-5">
		<div class="center-2">
			<img src="<?php echo get_template_directory_uri(); ?>/img/banner-participar.png">

			<div class="row">
				<div class="col-xs-8">
					<div class="chamada-form">

						<form novalidate name="formParticipar">
							<div class="row">
								<div class="col-xs-12 ">
									<b ng-show="alertaFacebook" style="color: white;">*Por favor, complete os seus dados</b>


<!-- 
									<div class="fb-login-button" data-max-rows="1" data-scope="public_profile,email" onlogin="getDadosFacebook()" data-size="large" data-button-type="continue_with" data-show-faces="true" data-auto-logout-link="false" data-use-continue-as="false"></div>
									 -->

									<br><br>
								</div>
								<div class="col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.name}">
										<input required type="text" class="form-control " ng-model="participante.name" placeholder="Nome">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.sobrenome}">
										<input required type="text" class="form-control " ng-model="participante.sobrenome" placeholder="Sobrenome">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.sexoId}">
										<select required class="form-control" ng-model="participante.sexoId" ng-options="model.Id as model.Descricao for model in sexos" required="required">
											<option value="" class="">Sexo</option>
										</select>
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.dtNasc.length == 10}">
										<input required  type="text" class="form-control date" ng-model="participante.dtNasc" placeholder="Data de Nascimento" name="">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.cpf.length == 14}">
										<input required type="text" ng-model="participante.cpf" class="form-control cpf" placeholder="CPF">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : endereco.cep.length == 9}">
										<input required type="text" ng-model="endereco.cep" required class="form-control cep" placeholder="CEP" ng-change="buscarCEP(endereco.cep)" ng-model-options="{updateOn: 'default blur',debounce: { 'default': 500, 'blur': 0 }}" name="">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-xs-9">
									<div class="form-group has-feedback" ng-class="{'has-success' : endereco.endereco}">
										<input required type="text" ng-model="endereco.endereco" class="form-control " placeholder="Endereço" name="">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="col-xs-3">
									<div class="form-group" ng-class="{'has-success' : endereco.enderecoNumero}">
										<input required ng-model="endereco.enderecoNumero" type="number" class="form-control"  placeholder="Número" ng-model="endereco.Numero" name="">
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : endereco.estado}">
										<select required class="form-control" ng-model="endereco.estado" ng-change="getCidades(endereco.estado)" ng-options="model.UF as model.Nome for model in estados" >
											<option value="">Estado</option>
										</select>
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : endereco.cidade}">
										<select required class="form-control" required="" ng-model="endereco.cidade" ng-disabled="endereco.estado == null || cidades && cidades.length == 0" ng-options="model.Nome as model.Nome for model in cidades">
											<option value="" class="" selected="selected">Cidade</option>
										</select>
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : endereco.bairro}">
										<input required type="text" class="form-control"  placeholder="Bairro" ng-model="endereco.bairro" name="">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : endereco.complemento}">
										<input type="text" class="form-control"  placeholder="Complemento" ng-model="endereco.complemento" name="">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3 ">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.DDD}">
										<input maxlength="2" type="text" class="form-control"  placeholder="DDD" ng-model="participante.DDD" name="">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>

								<div class="col-xs-6 col-sm-9 ">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.celular}">
										<input type="text" class="form-control tel"  placeholder="celular" ng-model="participante.celular" name="">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>

								<div class="col-sm-3 col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.DDD2}">
										<input maxlength="2" type="text" class="form-control"  placeholder="DDD" ng-model="participante.DDD2" name="">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>

								<div class="col-sm-9 col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.telefone}">
										<input type="text" class="form-control tel"  placeholder="telefone" ng-model="participante.telefone" name="">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>

								<div class="col-xs-12">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.email1}">
										<input required type="email" ng-model="participante.email1" class="form-control" placeholder="E-mail">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>
								</div>
								<div class="col-xs-12">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.email && participante.email == participante.email1, 'has-error' : participante.email && participante.email1 && participante.email != participante.email1  }">
										<input required type="email" ng-model="participante.email" class="form-control" placeholder="Confirmar E-mail">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
										<b ng-show="participante.email && participante.email1 && participante.email != participante.email1" class="text-danger">*emails diferentes</b>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.senha1}">
										<input required type="password" ng-model="participante.senha1" class="form-control" placeholder="Senha">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
									</div>

								</div>
								<div class="col-xs-6">
									<div class="form-group has-feedback" ng-class="{'has-success' : participante.senha && participante.senha == participante.senha1, 'has-error' : participante.senha && participante.senha1 && participante.senha != participante.senha1  }">
										<input type="password" ng-model="participante.senha" class="form-control" placeholder="Confirme a senha">
										<span class="glyphicon glyphicon-ok form-control-feedback"></span>
										<span class="glyphicon glyphicon-remove form-control-feedback"></span>
										<b ng-show="participante.senha && participante.senha1 && participante.senha != participante.senha1" class="text-danger">*senhas diferentes</b>
									</div>

								</div>
								<div class="col-xs-12 hide">
									<hr style=" margin: 5px 0px 6px 0px; ">
									<input class="hide" accept="image/png, image/jpg, image/jpeg, application/pdf" capture  id="file" file="file" type="file" name="">

									<div class="input-group" style="max-width: 70%;" onclick="document.getElementById('file').click()">
										<input ng-model="file.name" disabled="" type="text" class="form-control" placeholder="Faça o upload do cupom fiscal">

										<div class="input-group-btn">
											<button class="btn botao-3"  ></button>
										</div>

									</div>
									<br>

									<div class="alert alert-danger" ng-show="(file.size | formatarPontos ) > 2097152">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<strong>Atenção! </strong>Este file é maior que 2MB.
									</div>
									<p>
										Verifique se a imagem está legível e se mostra o cabeçalho 
										e os produtos comprados. Extensões: PDF, JPG ou JPEG 
										com no máximo 2MB.
									</p>

								</div>
								<div class="col-xs-12">
									<hr style=" margin: -5px 0px 6px 0px; ">
									<label>
										<input required type="checkbox" ng-model="aceito_regulameto"ng-true-value="true" ng-false-value="false" id="regulamento"> 
										Li e concordo com o <a href="javascript:$('#oregulamento').modal()" class="pointer" style="text-decoration: underline;">TERMO DE REGULAMENTO</a> desta promoção
									</label>
									<label>
										<input type="checkbox" ng-model="aceito_contato" ng-true-value="true" ng-false-value="false" id="termos"> 
										Aceito que os meus dados sejam utilizados pela 
										Perfetti Van Melle Brasil para futuras comunicações 
										de marketing e /ou comunicações promocionais
									</label>
								</div>
								<div class="col-xs-12">
									<hr style=" margin: -5px 0px 6px 0px; ">
									<span class="hide text-danger">*Favor selecionar o arquivo.</span>
									<button class="btn  botao-2 pull-right" ng-disabled="formParticipar.$invalid" ng-click="cadastrar(file); ">Cadastrar</button>
								</div>

								<div class="col-xs-12">
									<p ng-show="sucesso" class="animated fadeIn">
										<img src="<?php echo get_template_directory_uri()?>/img/icons/upload-com-sucesso.png">
										<span class="texto" style="font-size: 30px; position: relative; display: inline-block; top: 7px; ">UPLOAD FEITO COM SUCESSO!</span>

									</p>
								</div>
								<div class="col-xs-12">
									<div class="alertas"></div>
								</div>

							</div>
						</form>

					</div>
				</div>
			</div>
		</div>


	</div>
</section>


<!-- amazonaws -->
<script src="https://sdk.amazonaws.com/js/aws-sdk-2.207.0.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/controllers/Participar.js"></script>

<?php get_footer() ?>