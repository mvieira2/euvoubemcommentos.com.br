<?php get_header('logado') ?>

<section ng-controller="Upload-Nota-Fiscal">
	<div class="fundo-5">
		<div class="center-2">
			<img src="<?php echo get_template_directory_uri()?>/img/banner-upload-nota-fiscal.png">

			<form name="formNota">
				<div class="row">
					<div class="col-xs-8">
						<div class="chamada-form">
							<?php if (have_posts()) : ?>
								<?php while (have_posts()) : the_post(); ?>    
									<?php the_content(); ?>
								<?php endwhile; ?>
							<?php endif; ?>
							<hr>

							<div class="row">
								<div class="col-xs-2 text-center">
									<span class="number">1.</span>
								</div>
								<div class="col-xs-10 padding-0">
									<span class="texto">SELECIONE A NOTA FISCAL</span>
									<hr style="border-color: #01228f; margin: -5px 0px 11px 0px; ">
								</div>


								<div class="col-xs-10 padding-0 col-xs-push-2">

									<div class="row">
										<div class="col-xs-12">
											<div class="form-group">
												
												<select required class="form-control " ng-model="cupom.pdvId" name="razaoSocial"  ng-options="model.Nome as model.Nome for model in lista_atacadistas"  >
													<option value="">{{ !lista_atacadistas ? 'Carregando...' :  'Nome do atacadista'}}</option>
												</select>
											</div>
										</div>
										<div class="col-xs-6">
											
											<div class="form-group">
												<input required class="form-control" type="text" ng-model="cupom.numero" placeholder="N° do cupom fiscal" name="">
											</div>
										</div>
										<div class="col-xs-6 ">
											<div class="form-group">
												<input required class="form-control date" ng-model="cupom.data" type="text" placeholder="Data da compra" name="">
											</div>

										</div>
									</div>

								</div>
								<div class="col-xs-10 padding-0 col-xs-push-2">

									<input required class="hide" accept="image/png, image/jpg, image/jpeg, application/pdf" capture  id="file" file="file" type="file" name="">

									<div class="input-group" style="max-width: 84%;" onclick="document.getElementById('file').click()">
										<input ng-model="file.name" disabled="" type="text" class="form-control" placeholder="Faça o upload do cupom fiscal">

										<div class="input-group-btn">
											<button class="btn botao-3" ></button>
										</div>

									</div>

									<br>

									<div class="alert alert-danger" ng-show="(file.size | formatarPontos ) > 10485760">
										<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
										<strong>Atenção! </strong>Este file é maior que 10MB.
									</div>
									<p>
										Verifique se a imagem está legível e se mostra o cabeçalho 
										e os produtos comprados. Extensões: PDF, JPG ou JPEG 
										com no máximo 10MB.
									</p>

								</div>
								<div class="col-xs-10 padding-0 col-xs-push-2">
									<hr style="border-color: #01228f; margin: -5px 0px 6px 0px; ">
									<span class="hide text-danger">*Favor selecionar o arquivo.</span>
									<button class="btn  botao-2 pull-right" ng-disabled="formNota.$invalid || !file || file && ( file.size |  formatarPontos  ) > 10485760" ng-click="subirNotaFiscal(file); ">Enviar</button>
								</div>
								<div class="col-xs-10 padding-0 col-xs-push-2">
									<p ng-show="sucesso" class="animated fadeIn">
										<img src="<?php echo get_template_directory_uri()?>/img/icons/upload-com-sucesso.png">
										<span class="texto" style="font-size: 30px; position: relative; display: inline-block; top: 7px; ">UPLOAD FEITO COM SUCESSO!</span>
									</p>
									<div class="alertas"></div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</form>
		</div>




	</div>
</section>

<!-- Modal -->
<div id="modalOk" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body">
				<p style="text-align: center; font-size: 20px; font-family: arial, sans-serif;">
					Seus comprovantes fiscais foram enviados <br> com <b>sucesso</b> para o nosso sistema.
				</p>
				<p>
					<img style="display:block; border:0;"  src="<?php echo get_template_directory_uri()?>/img/email1_corpo.jpg">
				</p>
				<p style="text-align: center; font-size: 20px; font-family: arial, sans-serif;">
					Não esqueça de guardar todos os comprovantes fiscias cadastrados.
				</p>
			</div>

		</div>

	</div>
</div>

<!-- amazonaws -->
<script src="https://sdk.amazonaws.com/js/aws-sdk-2.207.0.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/controllers/Upload-Nota-Fiscal.js"></script>

<?php get_footer() ?>