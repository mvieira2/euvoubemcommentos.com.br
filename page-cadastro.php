<?php get_header('logado') ?>

<section class="page-padrao fundo-5 ng-cloak"  ng-controller="Cadastro">
	<div class="center-2">
		<div class="row">
			<div class="col-sm-12">
				<div class="alertas"></div>
			</div>
			<div class="col-sm-12">
				<ul class="nav nav-tabs nav-justified">
					<li class="active"><a href="#dados-pessoais">Dados Pessoais</a></li>
					<li><a href="#telefone">Telefone</a></li>
					<li><a href="#endereco">Endereço</a></li>
					<li><a href="#alterar-senha">Alterar senha</a></li>
				</ul>
				<br>
			</div>

			<div class="col-sm-12">

				<div class="tab-content">

					<div id="dados-pessoais"  class="tab-pane fade in active">
						<div class="row">
							<div class="col-sm-4 text-center">
								<p>
									<img ng-src="{{ img || participante.UrlFoto || '<?php echo get_template_directory_uri()?>/img/icons/foto-pessoa.png'}}" class="no-image img-thumbnail" alt="Cinque Terre">
								</p>
								<button onclick="document.getElementById('file').click()" class="botao-1 btn">Selecionar foto</button>
								<input class="hide" id="file" file="file" onchange="angular.element(this).scope().getImageBase64(angular.element(this))" type="file" accept="image/*" name="">
							</div>
							<div class="col-sm-8">

								<div class="row">
									<div class="col-xs-6">
										<div class="form-group">
											<input required type="text" ng-model="participante.name" class="form-control" placeholder="*Nome">
										</div>
										
									</div>
									<div class="col-xs-6">
										<div class="form-group">
											<input required type="text" ng-model="participante.sobrenome" class="form-control" placeholder="* Sobrenome">
										</div>
									</div>
									<div class="col-sm-12">
										<div class="form-group">
											<input required type="email" ng-model="participante.Email" class="form-control" placeholder="*E-mail">
										</div>
										
									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<input required type="text" ng-model="participante.CPF_CNPJ" class="form-control cpf" placeholder="*CPF">
										</div>
									</div>
									<div class="col-sm-6">

										<div class="form-group">
											<div class="input-group">
												<input type="text" ng-model="participante.Nascimento" class="form-control date" placeholder="*Nascimento">
												<div class="input-group-btn">
													<button class="btn btn-default">
														<i class="glyphicon glyphicon-calendar"></i>
													</button>
												</div>
											</div>
										</div>

									</div>

									<div class="col-sm-6">
										<div class="form-group">
											<select class="form-control" ng-model="participante.EstadoCivilId" ng-options="model.Id as model.Descricao for model in esdadosCivis" required="required">
												<option value="">*Estado civil</option>
											</select>
										</div>

									</div>
									<div class="col-sm-6">
										<div class="form-group">
											<select class="form-control" ng-model="participante.SexoId" ng-options="model.Id as model.Descricao for model in sexos" required="required">
												<option value="">Sexo</option>
											</select>
										</div>
									</div>

								</div>
							</div>
							<div class="col-sm-12">
								<hr>
								<button class="botao-2 btn pull-right" ng-click="salvarDadosPrincipais()">Salvar</button>
								<div class="clearfix"></div>
								<br>
							</div>
						</div>

					</div>

					<div id="telefone"  class="tab-pane fade ">

						<div class="row">
							<div class="col-sm-2">
								<div class="form-group">
									<select class="form-control" ng-model="telefone.TipoTelefoneId"  ng-options="model.Id as model.Descricao for model in tiposTelefone" required="required">
										<option value="">Tipo:</option>
									</select>
								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<input class="form-control" type="text"  minlength="2" maxlength="2" ng-model="telefone.DDD" placeholder="DDD" name="">

								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<input class="form-control tel" type="text" ng-model="telefone.Telefone" placeholder="Telefone" name="">

								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<input class="form-control" type="text" ng-model="telefone.Ramal" placeholder="Ramal" name="">

								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<label class="pointer">
										<input type="checkbox" ng-model="telefone.FlagTelPrincipal" name="">
										principal?
									</label>

								</div>
							</div>
							<div class="col-sm-2">
								<div class="form-group">
									<button class="botao-2 btn " ng-click="salvarTelefone(telefone)">Salvar</button>

								</div>
							</div>
							<div class="col-sm-12">
								<hr>

							</div>
							<div class="col-sm-12">
								<div class="table-responsive">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th><b>Tipo</b></th>
												<th class="text-center"><b>DDD</b></th>
												<th class="text-center"><b>Telefone</b></th>
												<th><b>Ramal</b></th>
												<th class="text-center"><b>Principal</b></th>
												<th class="text-center"><b>Editar</b></th>
												<th class="text-center"><b>Excluir</b></th>
											</tr>
										</thead>
										<tbody>

											<tr ng-show="!!!participante.Telefones.length">
												<td colspan="7">
													<p class="alert alert-warning text-center">
														Nenhum telefone cadastrado.
													</p>
												</td>
											</tr>

											<tr ng-repeat="tel in participante.Telefones track by $index" > 
												<td class="tipo">{{tel.TipoTelefone}}</td>
												<td class="text-center ddd"> {{ tel.DDD }} </td>
												<td class="text-center numero phone-number">{{ tel.Telefone }}</td>
												<td class="ramal">{{ tel.Ramal }} </td>
												<td class="text-center principal">
													<i class="fa fa-check" ng-show="tel.FlagTelPrincipal"></i>
												</td>
												<td class="text-center">
													<i class="fa fa-edit pointer" ng-click="editarTel(tel)" style="font-size: 20px;"></i>
												</td>
												<td class="text-center">
													<i class="fa fa-remove pointer" ng-click="excluirTelefone(tel)" style="font-size: 20px;"></i>
												</td>
											</tr>

										</tbody>
									</table>
								</div>

							</div>


						</div>

					</div>

					<div id="endereco"  class="tab-pane fade ">

						<div class="row">
							<form name="formEndereco">
								
								<div class="col-sm-3">
									<div class="form-group">
										<select class="form-control" ng-model="endereco.TipoEnderecoId" ng-options="model.Id as model.Descricao for model in tipoEnderecos" required="required">
											<option value="">Tipo</option>
										</select>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<input type="text" required class="form-control cep" placeholder="CEP" ng-change="buscarCEP(endereco.CEP)" ng-model-options="{updateOn: 'default blur',debounce: { 'default': 500, 'blur': 0 }}" ng-model="endereco.CEP" name="">
									</div>
								</div>

								<div class="col-sm-3">
									<div class="form-group">
										<select class="form-control" ng-model="endereco.UF" ng-change="getCidades(endereco.UF)" ng-options="model.UF as model.Nome for model in estados" required="required">
											<option value="">Estado</option>
										</select>
									</div>
								</div>

								<div class="col-sm-3">
									<div class="form-group">

										<select class="form-control" required ng-model="endereco.CidadeId"  ng-disabled="endereco.UF == null || cidades && cidades.length == 0" ng-options="model.Constante as model.Nome for model in cidades" required="required">
											<option value="">Cidade</option>
										</select>									
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<select class="form-control" required ng-model="endereco.TipoLogradouro"  ng-options="model.NomeTipo as model.NomeTipo for model in tipoLogradouros" required="required">
											<option value="">Tipo</option>
										</select>									
									</div>
								</div>

								<div class="col-sm-6">
									<div class="form-group">
										<input type="text" class="form-control " placeholder="Logradouro" ng-model="endereco.Logradouro" name="">
									</div>
								</div>
								<div class="col-sm-3">
									<div class="form-group">
										<input type="number" class="form-control"  placeholder="Número" ng-model="endereco.Numero" name="">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<input type="text" class="form-control"  placeholder="Bairro" ng-model="endereco.Bairro" name="">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<input type="text" class="form-control"  placeholder="Complemento" ng-model="endereco.Complemento" name="">
									</div>
								</div>

								<div class="col-sm-3">
									<div class="form-group">
										<label>
											<input type="checkbox" ng-model="endereco.FlagEnderecoPrincipal" ng-true-value="true" ng-false-value="false" name="">
											Principal?
										</label>
									</div>
								</div>
								<div class="col-sm-1">
									<div class="form-group">
										<button class="botao-2 btn pull-right" ng-click="salvarEnderecos(endereco, formEndereco.$valid)">Salvar</button>
									</div>
								</div>


							</form>
							<div class="col-sm-12">
								<hr>
								<div class="table-responsive">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th><b>Tipo</b></th>
												<th><b>Endereço</b></th>
												<th class="text-center"><b>CEP</b></th>
												<th><b>Cidade</b></th>
												<th class="text-center"><b>UF</b></th>
												<th class="text-center"><b>Principal</b></th>
												<th class="text-center"><b>Editar</b></th>
												<th class="text-center"><b>Excluir</b></th>
											</tr>
										</thead>
										<tbody>
											<tr ng-show="!!!participante.Enderecos.length">
												<td colspan="8">
													<p class="alert alert-warning text-center">
														Nenhum endereço cadastrado.
													</p>
												</td>
											</tr>
											<tr ng-repeat="end in participante.Enderecos track by $index">
												<td class="tipo">{{ end.TipoEndereco }}</td>
												<td class="logradouro">{{ end.Endereco }}</td>
												<td class="text-center cep">{{ end.CEP }}</td>
												<td class="cidade">{{ end.Cidade }}  </td>
												<td class="text-center uf">{{ end.UF }}</td>
												<td class="text-center principal"><i class="fa fa-check" ng-show='end.FlagEnderecoPrincipal'></i></td>
												<td class="text-center">
													<i class="fa fa-edit pointer" ng-click="editarEndereco(end)" style="font-size: 20px;"></i>
												</td>
												<td class="text-center">
													<i class="fa fa-remove pointer" ng-click="excluirEndereco(end)" style="font-size: 20px;"></i>
												</td>
											</tr>
										</tbody>
									</table>
								</div>


							</div>

						</div>
					</div>

					<div id="alterar-senha"  class="tab-pane fade ">
						<form novalidate name="formSenha">
							<div class="row">
								<div class="col-sm-4">
									<div class="form-group">
										<label>Senha Atual</label>
										<input required ng-model="senha.senhaAtual" class="form-control" type="password" name="">
									</div>
								</div>

								<div class="col-sm-4">
									<div class="form-group">
										<label>Senha Nova</label>
										<input required ng-model="senha.novaSenha1" class="form-control" type="password" name="">
									</div>
								</div>
								<div class="col-sm-4">
									<div class="form-group">
										<label>Confirme a Senha Nova</label>
										<input required ng-model="senha.novaSenha" class="form-control" type="password" name="">
										<span ng-show="senha.novaSenha1 && senha.novaSenha && senha.novaSenha1 != senha.novaSenha" style="color: #a94442;" class="text-danger"> *Senhas diferentes </span>
									</div>
								</div>
								<div class="col-sm-12">
									<hr>
									<button class="botao-2 btn pull-right" ng-disabled="formSenha.$invalid" ng-click="salvarSenha(formSenha.$valid)">Salvar</button>
									<div class="clearfix"></div>
									<br>
								</div>
							</div>
						</form>
					</div>
				</div>



			</div>


		</div>
	</div>


</section>

<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/controllers/Cadastro.js"></script>
<?php get_footer() ?>