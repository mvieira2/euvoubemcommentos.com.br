<?php get_header('logado') ?>

<section class="ng-cloak" ng-controller="Extrato">
	<div class="fundo-5">
		<div class="center-2">
			<img src="<?php echo get_template_directory_uri()?>/img/banner-extrato.png">

			<div class="row">
				<div class="col-xs-8">
					<div class="chamada-form">
						<?php if (have_posts()) : ?>
							<?php while (have_posts()) : the_post(); ?>    
								<?php the_content(); ?>
							<?php endwhile; ?>
						<?php endif; ?>
						
						<hr>

					</div>
				</div>
				<div class="col-xs-12">
					<div class="box-1 scroll">
						<ul class="nav nav-tabs">
							<li ng-click="filtrarMes(mes.mes)"  ng-repeat="mes in mesesFiltro track by $index"><a href="#">{{ mes.text }}</a></li>
						</ul>
						<div id="menu1">
							<br>
							<select  class="botao-4" ng-change="limparFiltro('pdvId')" ng-model="filtro.pdvId" ng-options="model.pdvId as model.pdvId for model in cuponsGeradosFiltered | noDuplicatePDV" >
								<option readonly value="">{{ filtro.pdvId ? 'Todas as redes' : 'Selecione a REDE'}}</option>
							</select>							
							<select  class="botao-4" ng-change="limparFiltro('cupom')" ng-model="filtro.cupom" ng-options="model.cupom as model.cupom for model in cuponsGeradosFiltered" >
								<option readonly value="">{{ filtro.cupom ? 'Todas as NFs' : 'Selecione a NF'}}</option>
							</select>		

							<div class="table-responsive">


								<table class="table table-hover" >
									<thead>
										<tr>
											<th style="width: 150px;">
												<span>
													N° da sorte
												</span>
											</th>
											<th style="width: 315px;">
												<span>
													Descrição ( n° da nota + data da compra)
												</span>
											</th>
											<th>
												<span>
													Rede
												</span>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="3" ng-show="dadosExtras == null || cuponsGeradosFiltered.length == 0">
												<p class="alert alert-warning">Nenhum número de sorteio por aqui...</p>
												
											</td>
										</tr>
										<tr ng-repeat="cupom in cuponsGerados | filter:filtro | mesAno:mesAno  as cuponsGeradosFiltered track by $index">
											<td class="text-center">{{ cupom.cupom }} </td>
											<td class="text-center">{{ cupom.nota ? cupom.nota+' - ' : ''   }} {{ cupom.data }} </td>
											<td class="text-center" title="{{ cupom.pdvId || '----'  }}"> {{ (cupom.pdvId || '----')   }} </td>
										</tr>
									</tbody>
								</table>
							</div>


						</div>

					</div>

				</div>
			</div>
		</div>




	</div>
</section>


<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/controllers/Extrato.js"></script>

<?php get_footer() ?>