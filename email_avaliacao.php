<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>E-mail</title>
</head>
<body>
	<center>
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<img style="display:block; border:0;"  src="<?php echo get_template_directory_uri()?>/img/email_top1.jpg">

				</td>
			</tr>
			<tr>
				<td>
					<p style="text-align: center; font-size: 20px; font-family: arial, sans-serif;">
						Seus comprovantes fiscais foram enviados <br> com <b>sucesso</b> para o nosso sistema.
					</p>
					<p>
						<img style="display:block; border:0;"  src="<?php echo get_template_directory_uri()?>/img/email1_corpo.jpg">
					</p>
					<p style="text-align: center; font-size: 20px; font-family: arial, sans-serif;">
						Não esqueça de guardar todos os comprovantes fiscias cadastrados.
					</p>
					<p style="text-align: center; font-size: 20px; font-family: arial, sans-serif;">
						Atenciosamente, <br>
						<b>Promoção Eu Vou Bem com Mentos e Fruit-tella</b>
					</p>
				</td>
			</tr>
			<tr>
				<td>
					<img style="display:block; border:0;"   src="<?php echo get_template_directory_uri()?>/img/email_footer.jpg">

				</td>
			</tr>
		</table>
	</center>
</body>
</html>
