<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8" />
	<title>E-mail</title>
</head>
<body>
	<center>
		<table border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<img style="display:block; border:0;"  src="<?php echo get_template_directory_uri()?>/img/email_top3.jpg">

				</td>
			</tr>
			<tr>
				<td>
					<p style="text-align: center; font-size: 20px; font-family: arial, sans-serif;">
						Veja abaixo os possíveis motivos: <br><br>
						● Não ter alcançado o mínimo de 03 (três) <br> displays (caixas) dos produtos participantes;

						<br><br>
						● As compras foram realizadas fora da data da promoção; <br><br>
						● Foto/Imagem do comprovante fiscal <br> não legível para validação;
						<br><br>
						● Realização de compra fora das lojas participantes.

						<br> <br>
						Consulte o regulameto no site da promoção: <br>
						<a target="_blank" href="https://www.euvoubemcommentos.com.br">www.euvoubemcommentos.com.br</a> <br>
						e confira as regras.


					</p>

					<p style="text-align: center; font-size: 20px; font-family: arial, sans-serif;">
						Atenciosamente, <br>
						<b>Promoção Eu Vou Bem com Mentos e Fruit-tella</b>
					</p>
				</td>
			</tr>
			<tr>
				<td>
					<img style="display:block; border:0;"   src="<?php echo get_template_directory_uri()?>/img/email_footer.jpg">

				</td>
			</tr>
		</table>
	</center>
</body>
</html>
