<?php get_header(); ?>



<section class="ng-cloak" >
	<div class="fundo-5">
		<div class="center-2">
			<img src="<?php echo get_template_directory_uri()?>/img/banner-produtos-participantes.png">


			<div class="row">

				<div class="col-xs-4 box-images">
					<div class="row">
						<div class="col-xs-12">
							<ul class="nav nav-tabs">

								<li class="active">
									<a href="#mentos-balas" data-toggle="tab">
										<img src="<?php echo get_template_directory_uri()?>/img/mentos-mint.png">
									</a>
								</li>
								<li>
									<a href="#mentos-goma" data-toggle="tab">
										<img src="<?php echo get_template_directory_uri()?>/img/mentos-goma.png">
									</a>
								</li>
								<li >
									<a href="#fruit-tella" data-toggle="tab">
										<img src="<?php echo get_template_directory_uri()?>/img/fruitt-tella.png">
									</a>
								</li>								
							</ul>

							
						</div>

					</div>
					
				</div>

				<div class="col-xs-8 box-textos">
					<div class="tab-content">
						
						<div  id="fruit-tella" class="tab-pane fade ">
							<h2>FRUIT-TELLA</h2>
							FRUITELLA MAST DOCE DE LEITE-40GX16STX24 <br>
							FRUITTELLA MAST FRAMBOESA 40GX16STX24DP <br>
							FRUITTELLA MAST GREGO - 40GX16STX24DP <br>
							FRUITTELLA MAST MORANGO - 40GX16STX24DP <br>
							FRUITTELLA MAST MSHAKE MOR-40GX16STX24DP <br>
							FRUITTELLA SWIRL CARAMELO-41GX15STX12DP <br>
							FRUITTELLA SWIRL GELATTO-41GX15STX12DP <br>
							FRUITTELLA SWIRL MORANGO - 41GX15STX12DP <br>
							FT LIFE CARAMELO - 18,9g x 12FT x 12DP <br>
							FT LIFE ESPRESSO - 18,9g x 12FT x 12DP <br>
							FT LIFE STRAWB CREAM - 18,9g x12FTx12DP <br>
						</div>

						<div id="mentos-goma" class="tab-pane fade  box-texto ">
							<h2>MENTOS GOMAS</h2>

							MENTOS PURE FRUIT MOR 28P - 56GX6GFX6DP <br>
							MT GARRAFA FUSION - 56G X 6GF X 6DP <br>
							MT PF UP2U GARRAFA 28P - 56G X 6GF X 6DP <br>
							P. FRESH STRONG MINT 3L 5P-8.5GX15FSX12D <br>
							PURE FRESH CHERRY GAR 28P-56GX6GFX6DP <br>
							PURE FRESH CINNAMON 3L 5P-8.5GX15FSX12DP <br>
							PURE FRESH MINT 3L 5P-8,5GX15FSX12DP <br>
							PURE FRESH MINT 3L 5P-8,5GX15FSX12DP <br>
							PURE FRESH MINT FT 7P - 10,5G X12FTX12DP <br>
							PURE FRESH MINT GAR 28P - 56G X 6GF X6DP <br>
							PURE FRESH SACHE MINT 4P -6G X 15FSX12DP <br>
							PURE FRESH SACHE WTGREEN 4P-6GX15FSX12DP <br>
							PURE FRESH SORT MONO 60P-1,5GX12DP <br>
							PURE FRESH WTGREEN FT 7P-10,5GX12FTX12DP <br>
							PURE FRESH WTGREEN GAR 28P - 56GX6GFX6DP <br>
							PURE FRESH/WHITE SORT POTE 160P-1,5GX4PT <br>
							PURE FRUIT 3L 5P-8,5GX15FSX12DP <br>
							PURE FRUIT SACHE TUT FRUTTI 6GX15FSX12DP <br>
							PURE FRUIT WATERMELON 3L 5P-8,5GX15FSX12 <br>
							PURE WHITE GARRAFA 28P - 56G X 6GF X 6DP <br>
							PURE WHITE TUTTI FRESH 3L 5P-8,5GX15FSX <br>
						</div>				

						<div id="mentos-balas" class="tab-pane fade in active  box-texto">
							<h2>MENTOS BALAS</h2>
							MENTOS 14pcs LIMITED EDITION MISTERY <br>
							MENTOS 14pcs LIMITED EDITION PARQUE DE DIVERSÕES <br>
							MENTOS ICE MINT 14P– 37,5G X 16ST X 24DP <br>
							MENTOS RAINBOW 5P - 13,5G X 30STX12DP <br>
							MENTOS RAINBOW 5P - 13,5G X 40ST X 12DP <br>
							MENTOS SORT POTE 5P-13,5G X 60ST X 12PT <br>
							MINI MT SORT POTE 5P - 10GX80STX12PT <br>
							MT 2 SHARE FRUIT 97PÇ - 5,4G X 12BAG <br>
							MT 2 SHARE MINT 97PÇ - 5,4G X 12BAG <br>
							MT BEATS SOR. FT 32P- 35G X 12FT X 18DP <br>
							MT BEATS SOR. SACHÊ 20GX18SCX12DP <br>
							MT BEATS SOR. SACHÊ 45Gx15SCX09DP <br>
							MT BIPACK KISS MENTA 35G X 2LT X 72MP <br>
							MT BIPACK KISS MENTA FORT 35GX2LTX72MP <br>
							MT BIPACK KISS MORANGO 35G X 2LT X 72MP <br>
							MT COLOR MIX FT SLIM 12P-32,1GX12FTX24 <br>
							MT CRAZY FRUIT FT SLIM 12P-32,1GX12FTX24 <br>
							MT CRAZYMINT FT SLIM 12P-32,1GX12FTX24DP <br>
							MT DUO BLACK ICE 14P - 37,5GX16STX24DP <br>
							MT FRUTAS 10P - 26,8G X 16ST X 36DP <br>
							MT FRUTAS 14P - 37,5G X 16ST X 24DP <br>
							MT FRUTAS FT SLIM 12P -32,1GX12FTX24DP <br>
							MT FRUTAS VERM 14P - 37,5GX16STX24DP <br>
							MT GREEN APPLE 14P - 37,5GX16STX24DP <br>
							MT ICE APPLE FT SLIM 12P-32,1GX12FTX24DP <br>
							MT ICE CHERRY FT SLIM12P-32,1GX12FTX24DP <br>
							MT ICE GRAPE FT SLIM 12P-32,1GX12FTX24DP <br>
							MT KISS MENTA FORTE FS 50P 35GX12LTX12DP <br>
							MT KISS MINT SF 50P – 35GX12LTX12DP <br>
							MT KISS STRAW SF 50P – 35GX12LTX12DP <br>
							MT MARACUJÁ COM MANGA 14P - 37,5GX16STX2 <br>
							MT MINT 10P - 26,8G X 16ST X 36DP <br>
							MT MINT 14P - 37,5G X 16ST X 24DP <br>
							MT MINT 5P-13,5G X 40ST X 12DP <br>
							MT MOR AZEDINHO 10P - 26,8GX16STX36DP <br>
							MT MOR IOG 14P - 37,5G X 16ST X 24DP <br>
							MT MOR YOG FT SLIM 12P-32,1GX12FTX24DP <br>
							MT RAINBOW 10P -  26,8G X 16ST X 36DP <br>
							MT RAINBOW 14P - 37,5G X 16ST X 24DP <br>
							MT STRAWBERRY MIX14P-37,5G X 16ST X 24DP <br>
							MT TUTI FRUTTI 5P - 13,5G X 40ST X 12DP <br>
							MT TUTTI FRUTTI 14P - 37,5GX16STX24DP <br>
							MT UVA 10P - 26,8GX16STX36DP <br>
						</div>

					</div>

				</div>






			</div>


		</div>

	</div>
</section>



<?php get_footer() ?>