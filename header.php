<!DOCTYPE html>
<html class="no-js" xmlns:ng="http://angularjs.org">

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title>
		<?php wp_title(''); ?>
		<?php if(wp_title('', false)) { echo ' :'; } ?>
		<?php bloginfo('name'); ?>
	</title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, user-scalable=no" />
	<!-- <meta name="viewport" content="width=980, user-scalable=yes" /> -->
	<meta name="description" content="<?php bloginfo('description'); ?>">


	<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/includes/animate/animate.css">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Chettan" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/includes/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/includes/bootstrap/css/bootstrap-datepicker.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/style.css?v1">

	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/angular/angular.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/angular/angular-sanitize.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/jquery/jquery.mask.min.js"></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/bootstrap/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/bootstrap/js/bootstrap-datepicker.pt-BR.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/bootstrap/js/bootstrap-notify.min.js"></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/config.js?v4"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/ng-file-upload.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/App.js"></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/moment/moment.min.js"></script>




	<?php wp_head(); ?>
</head>

<body <?php body_class('ng-cloak'); ?> ng-app="meuApp">

	<div id="load" style="display: none;">
		<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i>
	</div>

	<header class="mobile">
		<nav class="navbar navbar-default ">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">

					<ul class="nav navbar-nav">
						<li class="<?php echo is_home() &&  !$_GET['area'] ? 'active' : ''; ?>">
							<a href="<?php echo home_url(); ?>">HOME</a>
						</li>
						<li class="<?php echo $_GET['area'] == 'como-participar' ? 'active' : ''; ?>">
							<a href="<?php echo home_url('?area=como-participar'); ?>">COMO PARTICIPAR</a>
						</li>
						<li class="<?php echo $_GET['area'] == 'premios' ? 'active' : ''; ?>">
							<a href="<?php echo home_url('?area=premios'); ?>">PRÊMIOS</a>
						</li>
						<li class="<?php echo is_page('produtos-participantes') ? 'active' : ''; ?>">
							<a href="<?php echo home_url('/produtos-participante'); ?>">PRODUTOS PARTICIPANTES </a>
						</li>
						<li class="<?php echo is_page('participar') ? 'active' : ''; ?>">
							<a href="<?php echo home_url('/participar'); ?>"> PARTICIPAR </a>
						</li>
						<li class="<?php echo is_page('ganhadores') ? 'active' : ''; ?>">
							<a href="<?php echo home_url('/ganhadores'); ?>">GANHADORES</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		<form name="formHeaderLogin" class="form-header-login ng-cloak" ng-controller="Login">
			<div class="row">
				<div class="col-xs-12">
					<div class="row topotitle">
						<div class="borderRightBranco col-xs-6">
							<div class="details">
								<b>Primeiro acesso?</b>
								<br>
								<a href="<?php echo home_url('/participar'); ?>" style="text-decoration:underline;">Clique aqui</a> e cadastre-se
							</div>
						</div>
						<div class="col-xs-6 ">
							<div class="form-group">
								<div style="width: 60px;">
									<b style="margin-left: -5px;">
										OU
									</b>
									<a class="pointer" ng-click="loginFacebook();">
										<img src="<?php echo get_template_directory_uri()?>/img/icons/facebook.png" style="width:35px;">
									</a>

								</div>
							</div>
						</div>
						<div class="col-xs-12">
							<br>
							<label for="email">Faça o seu login: </label>
						</div>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						
						<input ng-model="login.login" type="text" class="form-control" id="email1" placeholder="E-mail">
						<b ng-show="errorLogin" class="animated fadeIn text-danger">*Login e/ou senha inválido </b>
					</div>

				</div>
				<div class="col-xs-12 ">
					<div class="form-group">
						<input ng-model="login.senha" type="password" class="form-control" id="pwd1" placeholder="Senha">
						<a href="<?php echo home_url('esqueci-minha-senha'); ?>">esqueci minha senha</a>
					</div>
				</div>
				<div class="col-xs-12">
					<div class="form-group">
						<button type="submit" class="btn botao-1" ng-click="logar(login, formHeaderLogin.$valid)">Enviar</button>
					</div>
				</div>


			</div>
		</form>

	</header>

	<header class="desktop header">

		<div class="comeca-menu ">
			<img class="mao-esquerda" src="<?php echo get_template_directory_uri()?>/img/mao-esquerda.png">
			<div class="fundo-1">
				<ul class="list-inline ">
					<li class="<?php echo is_home() &&  !$_GET['area'] ? 'active' : ''; ?>">
						<a href="<?php echo home_url(); ?>">HOME</a>
					</li>
					<li class="borderLeftBranco borderRightBranco <?php echo $_GET['area'] == 'como-participar' ? 'active' : ''; ?>">
						<?php if (is_home()) {  ?>
							<a href="javascript:animar('como-participar')">COMO PARTICIPAR</a>
						<?php  } else { ?>
							<a href="<?php echo home_url('?area=como-participar'); ?>">COMO PARTICIPAR</a>
						<?php  } ?>
					</li>
					<li class="borderRightBranco <?php echo $_GET['area'] == 'premios' ? 'active' : ''; ?>">
						<?php if (is_home()) {  ?>
							<a href="javascript:animar('premios')">PRÊMIOS</a>
						<?php  } else { ?>							
							<a href="<?php echo home_url('?area=premios'); ?>">PRÊMIOS</a>
						<?php  } ?>
					</li>
					<li class="<?php echo is_page('produtos-participantes') ? 'active' : ''; ?>">
						<a href="<?php echo home_url('/produtos-participantes'); ?>">PRODUTOS <br> PARTICIPANTES </a>
					</li>
					<li class="borderLeftBranco <?php echo is_page('participar') ? 'active' : ''; ?>">
						<a href="<?php echo home_url('/participar'); ?>"> PARTICIPAR </a>
					</li>
					<li class="borderLeftBranco <?php echo is_page('ganhadores') ? 'active' : ''; ?>">
						<a href="<?php echo home_url('/ganhadores'); ?>">GANHADORES</a>
					</li>
				</ul>

			</div>
			<img class="mao-direita" src="<?php echo get_template_directory_uri()?>/img/mao-direita.png">
		</div>

		<div class="center-2 ">
			<form name="formHeaderLogin" class="form-header-login ng-cloak" ng-controller="Login">
				<div class="row">
					<div class="col-xs-4">
						<div class="form-group">
							<label for="email">Faça o seu login: </label>
							<input ng-model="login.login" type="text" class="form-control" id="email" placeholder="E-mail">
							<b ng-show="errorLogin" style="font-size: 12px;" class="animated fadeIn text-danger">*Login e/ou senha inválido </b>
						</div>

					</div>
					<div class="col-xs-3 padding-0">
						<div class="form-group">
							<label for="pwd">
							</label>
							<input ng-model="login.senha" type="password" class="form-control" id="pwd" placeholder="Senha">
							<a href="<?php echo home_url('esqueci-minha-senha'); ?>">esqueci minha senha</a>
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
							<label>
							</label>
							<button type="submit" class="btn botao-1" ng-click="logar(login, formHeaderLogin.$valid)">Enviar</button>
						</div>
					</div>
					<div class="col-xs-1 padding-0 ">
						<label>	</label>
						<div class="form-group">
							<div style="width: 60px;">
								<b style="margin-left: -5px;">
									OU
								</b>
								<a class="pointer" ng-click="loginFacebook();">
									<img src="<?php echo get_template_directory_uri()?>/img/icons/facebook.png" style="width:35px;">
								</a>
								
							</div>
						</div>
					</div>
					<div class="col-sm-2">
						<div class="details">
							<b>Primeiro acesso?</b>
							<br>
							<a href="<?php echo home_url('/participar'); ?>" style="text-decoration:underline;">Clique aqui</a> e cadastre-se
						</div>
					</div>

				</div>
			</form>


		</div>

	</header>


	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.2&appId=340833323319830&autoLogAppEvents=1';
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/controllers/Login.js"></script>