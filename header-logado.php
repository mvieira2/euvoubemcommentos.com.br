<!DOCTYPE html>
<html class="no-js" xmlns:ng="http://angularjs.org">

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title>
		<?php wp_title(''); ?>
		<?php if(wp_title('', false)) { echo ' :'; } ?>
		<?php bloginfo('name'); ?>
	</title>

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, user-scalable=no" />
	<!-- <meta name="viewport" content="width=980, user-scalable=yes" /> -->
	<meta name="description" content="<?php bloginfo('description'); ?>">


	<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/fonts/font-awesome-4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/includes/animate/animate.css">
	<link href="https://fonts.googleapis.com/css?family=Baloo+Chettan" rel="stylesheet">

	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/includes/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/includes/bootstrap/css/bootstrap-datepicker.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/css/style.css">

	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/jquery/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/angular/angular.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/angular/angular-sanitize.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/jquery/jquery.mask.min.js"></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/bootstrap/js/bootstrap-datepicker.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/bootstrap/js/bootstrap-datepicker.pt-BR.min.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/bootstrap/js/bootstrap-notify.min.js"></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/config.js?v3"></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/ng-file-upload.js"></script>
	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/App.js"></script>

	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/includes/moment/moment.min.js"></script>

	<script type="text/javascript">
		var url_style = '<?php echo get_stylesheet_directory_uri(); ?>';
	</script>


	<?php wp_head(); ?>
</head>

<body <?php body_class('animated fadeIn ng-cloak'); ?> ng-app="meuApp">

	<div id="load" style="display: none;">
		<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw">
		</i>
	</div>

	<header class="mobile" ng-controller="Header">
		<nav class="navbar navbar-default ">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span> 
					</button>
				</div>
				<div class="collapse navbar-collapse" id="myNavbar">

					<ul class="nav navbar-nav">
						<?php if ( is_page('adm') || is_page('relatorios') ) {?>
								<li class="adm  borderRightBranco <?php echo is_page('adm') ? 'active' : ''; ?>" style="float: left;margin-left: 14%;">
								<a href="<?php echo home_url('/adm'); ?>">VALIDAÇÃO</a>
							</li>
							<li class="adm <?php echo is_page('relatorios') ? 'active' : ''; ?>">
								<a href="<?php echo home_url('/relatorios'); ?>">RELATÓRIOS</a>
							</li>

						<?php }; ?>
						<li class="<?php echo is_home() &&  !$_GET['area'] ? 'active' : ''; ?>">
							<a href="<?php echo home_url(); ?>">HOME</a>
						</li>
						<li class="<?php echo is_page('upload-de-nota-fiscal') ? 'active' : ''; ?>">
							<a href="<?php echo home_url('/upload-de-nota-fiscal'); ?>">UPLOAD DE NOTA FISCAL</a>
						</li>
						<li class="<?php echo is_page('extrato') ? 'active' : ''; ?>">
							<a href="<?php echo home_url('/extrato'); ?>">EXTRATO</a>
						</li>
						<li class="<?php echo is_page('cadastro') ? 'active' : ''; ?>">
							<a href="<?php echo home_url('/cadastro'); ?>">CADASTRO</a>
						</li>
						<li class="<?php echo is_page('ganhadores') ? 'active' : ''; ?>">
							<a href="<?php echo home_url('/ganhadores'); ?>">GANHADORES</a>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		<div class="user-details-mobile"> <a href="<?php echo home_url('/'); ?>">Olá, {{ usuario.nome ? usuario.nome : 'Nome e sobreome' }}</a> | <a class="pointer" ng-click="logout()">SAIR</a> </div>
	</header>



	<header class="desktop header header-logado" ng-controller="Header">

		<div class="comeca-menu ">
			<img class="mao-esquerda" src="<?php echo get_template_directory_uri()?>/img/mao-esquerda.png">
			<div class="fundo-1">
				<div class="user-details"> <a href="<?php echo home_url('/'); ?>">Olá, {{ usuario.nome ? usuario.nome : 'Nome e sobreome' }}</a> | <a class="pointer" ng-click="logout()">SAIR</a> </div>
				<ul class="list-inline ">
					<?php if ( is_page('adm') || is_page('relatorios') ) {?>
							<li class="adm  borderRightBranco <?php echo is_page('adm') ? 'active' : ''; ?>" style="float: left;margin-left: 14%;">
								<a href="<?php echo home_url('/adm'); ?>">VALIDAÇÃO</a>
							</li>
							<li class="adm <?php echo is_page('relatorios') ? 'active' : ''; ?>">
								<a href="<?php echo home_url('/relatorios'); ?>">RELATÓRIOS</a>
							</li>

					<?php }; ?>
					<li class="borderRightBranco <?php echo is_home() &&  !$_GET['area'] ? 'active' : ''; ?>">
						<a href="<?php echo home_url(); ?>">HOME</a>
					</li>
					<li class="borderRightBranco <?php echo is_page('upload-de-nota-fiscal') ? 'active' : ''; ?>">
						<a href="<?php echo home_url('/upload-de-nota-fiscal'); ?>">UPLOAD DE NOTA FISCAL</a>
					</li>
					<li class="borderRightBranco <?php echo is_page('extrato') ? 'active' : ''; ?>">
						<a href="<?php echo home_url('/extrato'); ?>">EXTRATO</a>
					</li>
					<li class="<?php echo is_page('cadastro') ? 'active' : ''; ?>">
						<a href="<?php echo home_url('/cadastro'); ?>">CADASTRO</a>
					</li>
					<li class="borderLeftBranco <?php echo is_page('ganhadores') ? 'active' : ''; ?>">
						<a href="<?php echo home_url('/ganhadores'); ?>">GANHADORES</a>
					</li>
				</ul>				
			</div>
			<img class="mao-direita" src="<?php echo get_template_directory_uri()?>/img/mao-direita.png">
		</div>

	</header>

	<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/controllers/Header.js"></script>