<?php get_header('logado') ?>

<section class=" fundo-5 ng-cloak" ng-controller="Relatorios" style="margin-top: -10px;padding: 20px">

	<div class="center-3" style="margin-top: 20px;">
		<div class="box-1 box-2" style="min-height: 300px; ">
			<div class="row">
				<div class="col-xs-12">
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#relatorio1">Relatorio 1</a></li>
						<li><a data-toggle="tab" href="#relatorio2">Relatório 2</a></li>
					</ul>

					<div class="tab-content">

						<div id="relatorio1" class="tab-pane fade in active">
							<h3>Relatório de participante por rede</h3>

							<div class="tabela-estilo-1 table-responsive-1 scroll">

								<table id="table1" class="table table-hover" style="width: 1200px;">
									<thead>
										<tr>
											<th>
												<span>
													Nome
												</span>
											</th>
											<th>
												<span>
													Rede
												</span>
											</th>
											<th>
												<span>
													CPF
												</span>
											</th>
											<th>
												<span>
													N° Nota fiscal
												</span>
											</th>
											<th>
												<span>
													Data Nota fiscal
												</span>
											</th>
											<th>
												<span>
													Qtd produtos comprados
												</span>
											</th>
										</tr>
									</thead>
									<tbody>	
										<tr ng-repeat="relatorio in relatorio1 track by $index">
											<td>{{ relatorio.nome }} </td>
											<td>{{ relatorio.rede }} </td>
											<td>{{ relatorio.cpf }} </td>
											<td>{{ relatorio.nota_numero }} </td>
											<td>{{ relatorio.nota_data }} </td>
											<td>{{ relatorio.qtd_produtos_comprados }} </td>
										</tr>
									</tbody>
								</table>
							</div>

							<hr>
							<a class="btn bg-primary pull-right" onclick="Utils.ExportarToExcell('#table1')">Exportar para planilha</a>

						</div>	
						<div id="relatorio2" class="tab-pane fade">
							<h3>Relatório de Cupons por Rede</h3>

							<div class="tabela-estilo-1 table-responsive-1 scroll">

								<table id="table2" class="table table-hover" style="width: 1200px;">
									<thead>
										<tr>
											<th>
												<span>
													Num cupom
												</span>
											</th>
											<th>
												<span>
													Data cupom
												</span>
											</th>
											<th>
												<span>
													Rede
												</span>
											</th>
											<th>
												<span>
													N° Nota fiscal
												</span>
											</th>
											<th>
												<span>
													Data Nota fiscal
												</span>
											</th>
											<th>
												<span>
													Nome
												</span>
											</th>
											<th>
												<span>
													CPF
												</span>
											</th>
										</tr>
									</thead>
									<tbody>
										<tr ng-repeat="relatorio in relatorio2 track by $index">
											<td> {{ relatorio.cupom_numero }} </td>
											<td> {{ relatorio.cupom_data }} </td>
											<td> {{ relatorio.cupom_rede }} </td>
											<td> {{ relatorio.nota_num }} </td>
											<td> {{ relatorio.nota_data }} </td>
											<td> {{ relatorio.nome }} </td>
											<td> {{ relatorio.cpf }} </td>
										</tr>
									</tbody>
								</table>
							</div>

							<hr>
							<a class="btn bg-primary pull-right" onclick="Utils.ExportarToExcell('#table2')">Exportar para planilha</a>

						</div>
					</div>

					<pre class="hide">

						Relatório de Cupons por Rede
						{{ relatorio2 | json }}

						<hr style="border-color: blue; ">

						Relatório de participante por rede

						{{ relatorio1 | json }}

						<hr style="border-color: blue; ">
						{{ ganhadores | json}}

						<hr style="border-color: blue; ">
					</pre>

				</div>

			</div>
		</div>
	</div>
</section>

<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/controllers/Relatorios.js"></script>

<?php get_footer() ?>