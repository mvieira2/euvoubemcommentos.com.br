<?php get_header('logado') ?>

<section class=" fundo-5 ng-cloak" ng-controller="ADM" style="min-height: 300px; margin-top: -10px;padding: 20px">

	<div class="center-3" style="margin-top: 20px;">
		<div class="box-1 box-2">
			<div class="row">
				<div class="col-xs-12">
					<ul class="nav nav-tabs">
						<li ng-click="filtrarMes(mes.mes)"  ng-repeat="mes in mesesFiltro track by $index"><a href="#">{{ mes.text }}</a></li>
					</ul>
					<div id="menu1">
						<br>
						<select  class="botao-4" ng-change="limparFiltroData(filtro.nota)" ng-model="filtro.nota.data" ng-options="model.nota.data as model.nota.data for model in notasFiltered | noDuplicateData" >
							<option readonly value="">{{ filtro.nota.data ? 'Todas as datas' : 'Data'}}</option>
						</select>
						<select  class="botao-4" ng-change="limparFiltroPDVid(filtro)" ng-model="filtro.PDVId" ng-options="model.PDVId as model.PDVId for model in notasFiltered | noDuplicatePDV" >
							<option readonly value="">{{ filtro.PDVId ? 'Todas as redes' : 'Redes'}}</option>
						</select>
						<div class="tabela-estilo-1 table-responsive-1 scroll">

							<table class="table table-hover" >
								<thead>
									<tr>
										<th>
											<span>
												Nome
											</span>
										</th>
										<th>
											<span>
												Rede
											</span>
										</th>
										<th>
											<span>
												Data
											</span>
										</th>
										<th colspan="2">
											<span>
												Infos.
											</span>
										</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-show="notasFiltered && notasFiltered.length == 0">
										<td style="background:#fff;" colspan="4" >
											<p class="alert alert-warning">Nenhum cadastro por enquanto...</p>
										</td>
									</tr>
									<tr title="Usuario id: {{ nota1.UsuarioId }}" ng-repeat="nota1 in notasGeradas | orderBy:nota1.nota.data:true | filter:filtro | mesAno:mesAno as notasFiltered  track  by $index " ng-class="{'active' : nota1.nota.numero == nota.nota.numero}">
										<td>{{ nota1.Nome }} </td>
										<td>{{ nota1.PDVId }} </td>
										<td>{{ nota1.nota.data || '---' }} </td>
										<td class="text-center" colspan="2">
											<a class="pointer" ng-click="editarNota(nota1);">VER NOTA</a>
											<i class="fa fa-eye pointer" style="font-size: 20px;"></i>
											<i ng-show="!nota1.nota.rejeitado && nota1.nota.cuponsGerados.length " class="fa fa-check-circle-o" style="font-size: 20px;color: green;"></i>
											<i ng-show="nota1.nota.rejeitado" class="fa fa-close" style="font-size: 20px;color: red;"></i>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>

					<hr>
				</div>
			</div>

			<div class="col-xs-12 area-adm" ng-show="nota">	

				<br>


				<pre class="hide">
					{{ notasGeradas | json }}
				</pre>

				<div class="row">

					<div class="col-sm-8" style="border-right: 1px solid #d1d3d4;">

						<div class="row linha-notas">
							<div class="col-xs-12">
								<div class="row">
									<div class="col-xs-6">
										Número da Nota: <strong> {{ nota.nota.numero || '---' }}</strong>
									</div>
									<div class="col-xs-6 text-right">
										Data: {{ nota.nota.data || '---' }}
									</div>
								</div>
								<hr>
							</div>
							<div class="col-xs-5 arquivo">

								<img ng-if="isImage(nota.nota.nota_link)" ng-src="{{ nota.nota.nota_link || 'https://placehold.it/242x360text=No%20image'}}" style="max-width: 100%;height: auto;">
								<iframe ng-if="nota.nota.nota_link.indexOf('.pdf') > -1" style="width: 100%; height: 400px;" src="{{ 'https://docs.google.com/gview?url='+ nota.nota.nota_link  + '&embedded=true' | trustThisUrl}}" border="0"></iframe>

								<div class="clearfix"></div>
								<a target="_blank" ng-href="{{nota.nota.nota_link}}">Ver tamanho original</a>

							</div>
							<div class="col-xs-6">
								<div class="linha-produtos-unidades" ng-repeat="produto in nota.nota.produtos track by $index">
									<div class="row">
										<div class="col-xs-5 padding-0">
											<select  class="form-control botao-4" ng-model="produto.item" ng-options="model as model for model in produtosDisplays" >
												<option selected value="">Produto</option>
											</select>

										</div>
										<div class="col-xs-6">
											<input type="number" min="1" max="1000" ng-model="produto.Qtd" class="form-control botao-4" ng-change="atualizaQuantidadeCupons()" placeholder="Quantidade" name="">

										</div>
										<div class="col-xs-1 padding-0">

											<i ng-show="nota.nota.produtos.length > 1 && $index == nota.nota.produtos.length - 2 " ng-click="removerProduto(nota.nota.produtos, $index)" class="pointer glyphicon glyphicon-minus-sign" style="font-size: 19px; color: #01218e; margin: 6px 0px;"></i> 
											<i ng-show="$index == nota.nota.produtos.length - 1" ng-click="addProduto(nota.nota.produtos)" class="pointer fa fa-plus-circle	" style="font-size: 21px;color: #01218e;margin: 6px 0px;"></i> 
										</div>

									</div>



								</div>


							</div>
							<div class="col-xs-12">
								<hr>
								
							</div>
						</div>

					</div>


					<div class="col-xs-4 justificativa" style="border-left: 1px solid #d1d3d4;border-bottom: 1px solid #d1d3d4;">
						<div class="form-group">
							<p>
								<b>
									Total: <span class="badge">{{ getQtdNumeroDaSorte()  }}</span> cupo{{ getQtdNumeroDaSorte() > 1 ? 'ns' : 'm'}}
								</b>
							</p>
							<hr>
						</div>
						<div class="form-group">
							<label>
								<span class="badge">Rejeitar</span> 
								<input ng-disabled="nota.nota.campos_lock" ng-model="nota.nota.rejeitado" ng-true-value="true" ng-false-value="false" type="checkbox" name="">
							</label>
						</div>
						<div class="form-group">
							<textarea ng-disabled="nota.nota.campos_lock" ng-model="nota.nota.justificativa" style="width: 100%; height: 150px;" placeholder="Justificativa"></textarea>
							<hr>
						</div>
						<div class="form-group">
							<button class="botao-5" ng-click="editarStatus()">Editar</button>
							<button ng-show="nota.nota.rejeitado" ng-disabled="nota.nota.campos_lock" ng-click="finalizar()" class="botao-6 pull-right">Finalizar</button>
							<button ng-show="!nota.nota.rejeitado" ng-click="gerarNumeroDaSorte(nota)" class="botao-7 pull-right">Gerar cupom</button>

						</div>
						<div class="form-group">
							<div class="alertas"></div>
						</div>

						<div class="form-group" ng-show="nota.nota.cuponsGerados && nota.nota.cuponsGerados.length>0">
							<hr>
							<div class="list-group">
								<a class="list-group-item disabled">Números da sortes gerados</a>
								<a class="list-group-item" ng-repeat="cupom in nota.nota.cuponsGerados track by $index" data-*ng-click="nota.nota.cuponsGerados.splice($index,1)">{{ cupom.numero }} - {{ cupom.data }}</a>
							</div>
							<hr>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</section>

<script id="naoaprovado" type="text/template">
	<?php get_template_part('email_nao_aprovado'); ?>
</script>


<script id="aprovado" type="text/template">
	<?php get_template_part('email_aprovado'); ?>
</script>

<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/controllers/ADM.js"></script>


<?php get_footer() ?>