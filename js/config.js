var WS_Url = 'http://homolog.hotshoponline.com.br/Comunicacao_WS/';
//var WS_Url = '/api/?path=/Comunicacao_WS/'; // url para solicitação com https.


var Portal_Url = 'https://www.euvoubemcommentos.com.br/';
var url_site = 'http://localhost/wordpress/euvoubemcommentos/'
//var url_site = 'http://www.euvoubemcommentos.com.br/'
//var url_site = 'http://euvoubemcommentos.portalhsol.com.br/'
//var url_site = window.location.origin+'/';

var WS = {
	Configuracoes: {
		Svc: WS_Url+'WsConfiguracoes.svc',
		Url: WS_Url+'WsConfiguracoes.svc/ObterConfiguracaoUrl',
		PDV: WS_Url+'WsConfiguracoes.svc/ObterPontoDeVenda',
		ObterRegulamento: WS_Url+'WsConfiguracoes.svc/ObterRegulamento',
		ObterRegulamentoPendenteAceite: WS_Url+'WsConfiguracoes.svc/ObterRegulamentoPendenteAceite',
		AceitarRegulamento: WS_Url+'WsConfiguracoes.svc/AceitarRegulamento',
		ObterPesquisa: WS_Url+'WsConfiguracoes.svc/ObterPesquisasDisponiveis',
		SalvarResposta: WS_Url+'WsConfiguracoes.svc/SalvarResposta',
		ObterCargos: WS_Url+'WsConfiguracoes.svc/ObterCargos',
		ListarRegionais: WS_Url+'WsConfiguracoes.svc/ListarRegionais',
		ObterPDVsDeUsuario: WS_Url+'WsConfiguracoes.svc/ObterPDVsDeUsuario',
	},
	Autenticar: {
		Svc: WS_Url+'WsAutenticar.svc',
		Logar: WS_Url+'WsAutenticar.svc/Logar',
		Usuario: WS_Url+'WsAutenticar.svc/AutenticarUsuario',
		ValidarToken: WS_Url+'WsAutenticar.svc/ValidarToken',
		ValidarUsuario: WS_Url+'WsAutenticar.svc/ValidarUsuarioLogado',
		ValidarSenha: WS_Url+'WsAutenticar.svc/ValidarSenha',
		AlterarSenha: WS_Url+'WsAutenticar.svc/AlterarSenha',
		Cadastrar: WS_Url+'WsAutenticar.svc/Cadastrar',
		EsqueciMinhaSenha: WS_Url+'WsAutenticar.svc/EsqueciMinhaSenha',
		ObterChaveAcesso: WS_Url+'WsAutenticar.svc/ObterChaveAcessoHSOL',
		LogarComTokenHSOL: WS_Url+'WsAutenticar.svc/LogarComTokenHSOL',
		CadastrarGigantes: WS_Url+'WsAutenticar.svc/CadastrarGigantes',		
		ValidarLoginFacebook: WS_Url+'WsAutenticar.svc/ValidarLoginFacebook',		
	},
	Participantes: {
		Svc: WS_Url+'WsParticipantes.svc',
		ObterDadosPremiado: WS_Url+'WsParticipantes.svc/ObterDadosPremiado',
		Saldo: WS_Url+'WsParticipantes.svc/ObterSaldoParticipante',
		Perfil: WS_Url+'WsParticipantes.svc/ObterPerfil',
		Extrato: WS_Url+'WsParticipantes.svc/ObterExtratoParticipante',
		Foto: WS_Url+'WsParticipantes.svc/ObterFotoParticipante',
		Telefone: WS_Url+'WsParticipantes.svc/ObterTelefonesParticipante',
		Endereco: WS_Url+'WsParticipantes.svc/ObterEnderecosParticipante',
		EstadoCivil: WS_Url+'WsParticipantes.svc/ObterEstadosCivis',
		Sexo: WS_Url+'WsParticipantes.svc/ObterSexos',
		TiposTelefone: WS_Url+'WsParticipantes.svc/ObterTiposDeTelefone',
		TiposEndereco: WS_Url+'WsParticipantes.svc/ObterTiposDeEndereco',
		TiposLogradouro: WS_Url+'WsParticipantes.svc/ObterTiposDeLogradouro',
		EnderecoEstados: WS_Url+'WsParticipantes.svc/ObterEnderecoEstados',
		EnderecoCidades: WS_Url+'WsParticipantes.svc/ObterEnderecoCidades',
		SalvarDados: WS_Url+'WsParticipantes.svc/SalvarParticipante',
		SalvarFoto: WS_Url+'WsParticipantes.svc/SalvarFotoParticipante',
		SalvarTelefone: WS_Url+'WsParticipantes.svc/SalvarTelefoneParticipante',
		SalvarEndereco: WS_Url+'WsParticipantes.svc/SalvarEnderecoParticipante',
		ExcluirTelefone: WS_Url+'WsParticipantes.svc/DesabilitarTelefoneParticipante',
		ExcluirEndereco: WS_Url+'WsParticipantes.svc/DesabilitarEnderecoParticipante',
		BuscarCEP: WS_Url+'WsParticipantes.svc/BuscarInformacoesDeCEP',
		ObterListaTimes: WS_Url+'WsParticipantes.svc/ObterListaTimes',
		ObterListaCategoriasMusica: WS_Url+'WsParticipantes.svc/ObterListaCategoriasMusica',
		SalvarDadosOpcionais: WS_Url+'WsParticipantes.svc/SalvarDadosOpcionais',
		EnviarArquivo: WS_Url+'WsParticipantes.svc/EnviarArquivo',
		SalvarFoto: WS_Url+'WsParticipantes.svc/SalvarFotoParticipante',
		ObterMetas: WS_Url+'WsParticipantes.svc/ObterMetas',
		ObterMetasPersonalizadas: WS_Url+'WsParticipantes.svc/ObterMetasPersonalizadas',
		SalvarMetas: WS_Url+'WsParticipantes.svc/SalvarMetas',
		BloquearParticipante: WS_Url+'WsParticipantes.svc/BloquearParticipante',
		ObterRanking: WS_Url+'WsParticipantes.svc/ObterRanking',
		ObterRelatorioConversoes: WS_Url+'WsParticipantes.svc/ObterRelatorioConversoes',
		EfetuarDebitoExtratoPromocional: WS_Url+'WsParticipantes.svc/EfetuarDebitoExtratoPromocional',
		EnviarFotoPerfil: WS_Url+'WsParticipantes.svc/EnviarFotoPerfil',
		BonusAniversario: WS_Url+'WsParticipantes.svc/RegistrarBonusAniversario',
		SalvarDadosExtras: WS_Url+'WsParticipantes.svc/SalvarDadosExtras',
		ListarUsuarios: WS_Url+'WsParticipantes.svc/ListarUsuarios',
		EnviarEmailGenerico: WS_Url+'WsParticipantes.svc/EnviarEmailGenerico',
		ObterEnderecoCidadesComDistinct: WS_Url+'WsParticipantes.svc/ObterEnderecoCidadesComDistinct',
	},
	Quiz: {
		Svc: WS_Url+'WsQuiz.svc',
		BuscarQuiz: WS_Url+'WsQuiz.svc/BuscarQuiz',
		BuscarPerguntas: WS_Url+'WsQuiz.svc/BuscarPerguntas',
		ResponderPerguntas: WS_Url+'WsQuiz.svc/ResponderPerguntas',
		BuscarResumoParticipacao: WS_Url+'WsQuiz.svc/BuscarResumoParticipacao',
		BuscarResumoParticipacaoERanking: WS_Url+'WsQuiz.svc/BuscarResumoParticipacaoERanking',
		FinalizarQuiz: WS_Url+'WsQuiz.svc/FinalizarQuiz',
		RefazerQuiz: WS_Url+'WsQuiz.svc/RefazerQuiz',
		RelatorioParticipacao: WS_Url+'WsQuiz.svc/RelatorioParticipacao',
		RegistrarInicioResposta: WS_Url+'WsQuiz.svc/RegistrarInicioResposta',
		ObterRankingQuiz: WS_Url+'WsQuiz.svc/ObterRankingQuiz',
	},
	Sair: function() {
		window.localStorage.clear();
		$(location).attr('href', url_site);
	},
	Post: function(url, callback, data) {
		
		var params = {
			type: "POST",
			url: url,
			headers: { 'Content-Type': 'application/json' },
			dataType: "json",
		}

		if (data)
			params.data = data;	

		if (callback)
			params.success = callback;
		
		$.ajax(params);
	}
};

var Utils = {

	VerificarLogin: function() {
		if ( !localStorage.getItem('oAuth') ) {
			$(location).attr('href', url_site);
		} else {
			return true;
		}
	},

	ObterIP: function() {
		var ip = '';
		$.ajax({
			url: '//api.ipify.org?format=json&callback=?',
			async: false,
			success: function (data) {
				ip = data['ip'];
			},
			error: function (data) {
				ip = '';
			}
		});
		return ip;
	},

	isValidEmail: function(emailAddress) {
		var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
		return pattern.test(emailAddress);
	},

	Masks: function() {

		$('.date').unmask().mask('00/00/0000');
		$('.cep').unmask().mask('00000-000');
		$('.cpf').unmask().mask('000.000.000-00');
		$('.cnpj').unmask().mask('00.000.000/0000-00');
		$('.number').unmask().mask('999.999.999.999.999.999', {
			reverse: true
		});
		$('.money').unmask().mask('999.999.999.999.999,99', {
			reverse: true
		});

		if( $('.cpfcnpj').length > 0 ) {
			if ( $('.cpfcnpj').unmask().val().length >= 14 ) {
				$('.cpfcnpj').unmask().mask('00.000.000/0000-00');
			} else {
				$('.cpfcnpj').unmask().mask('000.000.000-00');
			}
		}


		var SPMaskBehavior = function (val) {
			return val.replace(/\D/g, '').length === 9 ? '00000-0000' : '0000-00009';
		},
		spOptions = {
			onKeyPress: function(val, e, field, options) {
				field.mask(SPMaskBehavior.apply({}, arguments), options);
			}
		};
		$('.tel').unmask().mask(SPMaskBehavior, spOptions);

		var DDDSPMaskBehavior = function (val) {
			return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
		},
		DDDSpOptions = {
			onKeyPress: function(val, e, field, options) {
				field.mask(DDDSPMaskBehavior.apply({}, arguments), options);
			}
		};
		$('.phone-number-ddd').unmask().mask(DDDSPMaskBehavior, DDDSpOptions);
	},
	ExportarToExcell: (function() {
		var uri = 'data:application/vnd.ms-excel;base64,', 
		template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><meta http-equiv="content-type" content="application/vnd.ms-excel; charset=UTF-8"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>', 
		base64 = function(s) { return window.btoa(unescape(encodeURIComponent(s))) }, 
		format = function(s, c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
		return function(table, name) {
			if (!table.nodeType) table = document.querySelector(table);

			var dt = new Date();
			var day = dt.getDate();
			var month = dt.getMonth() + 1;
			var year = dt.getFullYear();
			var hour = dt.getHours();
			var mins = dt.getMinutes();
			var sec = dt.getMilliseconds();
			var postfix = year + "-" + month + "-" + day + "-" + hour + "-" + mins+"-"+sec;
			var ctx = {worksheet: name || 'tabela-'+postfix, table: table.innerHTML}

			var link1 = document.createElement("a");
			link1.download = ctx.worksheet+".xls";
			link1.href = uri + base64(format(template, ctx));
			link1.click();

			delete link1;

		}

	})(),
	IconeLoaderOn : function () {
		$('#load').css({"opacity":"1", "display":"block"});
	},
	IconeLoadeOff : function () {
		$('#load').css({"opacity":"0", "display":"none"});
	}
}

var Usuario = {}, PDV = {}, MeusDados = {};
var libphonenumber = false;

if (jQuery) {
	$ = jQuery;
}