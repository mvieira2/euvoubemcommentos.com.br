meuApp.controller("Footer", function($window, $scope, $rootScope, $location,$sce, $http, $timeout, API){


	$scope.loadRegulamento = async function () {

		try {
			$scope.regulamento = await API.post(WS.Configuracoes.ObterRegulamento, { usuarioId : JSON.parse(localStorage.getItem('Usuario'))['UsuarioId'] });
			$scope.regulamento = $scope.regulamento.ObterRegulamentoResult || '';

			$scope.regulamentoText = $scope.regulamento.Texto;

			$scope.botaoAceite = false;

			$scope.$apply();


		} catch(e) {

			$scope.regulamentoIsStatic = true;
			// statements
			console.log(e);
		}

	}


	$scope.load2 = async function () {


		try {
			let regulamentosPendentes = await API.post(WS.Configuracoes.ObterRegulamentoPendenteAceite, { usuarioId : JSON.parse(localStorage.getItem('Usuario'))['UsuarioId'] });
			regulamentosPendentes = regulamentosPendentes.ObterRegulamentoPendenteAceiteResult ? regulamentosPendentes.ObterRegulamentoPendenteAceiteResult : []

			if ( angular.isObject( regulamentosPendentes ) && regulamentosPendentes.RegulamentoId  ) {

				$scope.regulamento = regulamentosPendentes;

				$scope.regulamentoText = regulamentosPendentes.Texto;

				$scope.botaoAceite = true;

				$('#oregulamento').modal();

			}

			else if ( angular.isArray(regulamentosPendentes) && regulamentosPendentes.length > 0 ) {
				$scope.regulamentoText = regulamentosPendentes[0].Texto;

				$scope.regulamento = regulamentosPendentes;

				$('#oregulamento').modal();

				$scope.botaoAceite = true;

			}

			else {

				$scope.loadRegulamento();

			}


			$scope.$apply();

		} catch(e) {
			$scope.regulamentoIsStatic = true;

			// $timeout( async function () {
			// 	$scope.regulamentoEstatico = await API.get( url_site + '/regulamento');
				
			// 	$scope.$apply();
				
			// },500);


			console.log(e);
		}

	}


	$scope.aceitarRegulamento = async function () {

		let result = await API.post( WS.Configuracoes.AceitarRegulamento, { usuarioId : JSON.parse(localStorage.getItem('Usuario'))['UsuarioId'], regulamentoClienteCampanhaId : $scope.regulamento.RegulamentoClienteCampanhaId  });

		if (result.AceitarRegulamentoResult.OperacaoRealizadaComSucesso) {
			$('#oregulamento').modal('hide');
		}

		$scope.loadRegulamento();
	}


	$scope.load2();


});