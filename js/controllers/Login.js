meuApp.controller("Login", function($window, $rootScope, $scope,$location, $http, $timeout, API){

	$scope.login = {
		"url":"http://www.euvoubemcommentos.com.br",
		"login":null,
		"senha":null
	};


	$rootScope.logar = function (model, isValid) {
		$scope.errorLogin = false;

		if (!isValid) {
			$scope.errorLogin = true;
			return;
		}



		$http({
			url: WS.Autenticar.Logar,
			method: 'POST',
			data: model,
		}).then( function (r) {
			

			if (r.data.OperacaoRealizadaComSucesso) {

				localStorage.setItem('oAuth', r.data.MensagemSucesso);


				$http({
					url: WS.Configuracoes.Url,
					method: 'POST',
					data: {'url': Portal_Url},
				}).then(function (r2) {

					let ClienteCampanhaId = null;

					if (r2.data.ObterConfiguracaoUrlResult && r2.data.ObterConfiguracaoUrlResult.ClienteCampanhaId) {
						ClienteCampanhaId = r2.data.ObterConfiguracaoUrlResult.ClienteCampanhaId
					}

					$http({
						url: WS.Autenticar.ValidarToken,
						method: 'POST',
						data: {"accessToken":localStorage.getItem('oAuth'),"ClienteId": ClienteCampanhaId },
					}).then(function (r3) {
						localStorage.setItem('Usuario', JSON.stringify(r3.data));


						if (r3.data.PerfilNome.toLowerCase() == "adm cliente" ) {
							$window.location.href = decodeURIComponent($location.absUrl().split('?redirect=')[1] || url_site+'adm');
							return
						}
						else if (r3.data.PerfilNome.toLowerCase() == "participante" && !r3.data.PrimeiroAcesso) {
							$window.location.href = decodeURIComponent($location.absUrl().split('?redirect=')[1] || url_site+'cadastro');
							return
						}

						$window.location.href = decodeURIComponent($location.absUrl().split('?redirect=')[1] || url_site+'upload-de-nota-fiscal');
						return;
						
					})

				})

				return;

			}

			$scope.errorLogin = true;

			

		}, function (error) {
			
			$scope.errorLogin = true;
			return  error.data;
		});
	}



	$rootScope.loginFacebook = function () {
		
		FB.login(function(response) {
			if (response.authResponse) {

				$http({
					url: WS.Configuracoes.Url,
					method: 'POST',
					data: {'url': Portal_Url},
				}).then(async function (r2) {

					const clienteCampanhaId = 1502;

					if (r2.data.ObterConfiguracaoUrlResult && r2.data.ObterConfiguracaoUrlResult.ClienteCampanhaId) {
						clienteCampanhaId = r2.data.ObterConfiguracaoUrlResult.ClienteCampanhaId
					}

					const result = await API.post(WS.Autenticar.ValidarLoginFacebook, {
						campanhaId : clienteCampanhaId,
						token : response.authResponse.accessToken

					});

					if (!result.OperacaoRealizadaComSucesso) {

						FB.api(
							'/me',
							'GET',
							{"fields":"id,name,birthday,email,first_name,last_name,gender,photos{picture},picture{url}"},
							function(response2) {
								sessionStorage.setItem('facebookUser', JSON.stringify(response2));
								$window.location.href = decodeURIComponent($location.absUrl().split('?redirect=')[1] || url_site+'participar');
							});

						return;

					}

					else if (result.OperacaoRealizadaComSucesso) {
						$http({
							url: WS.Autenticar.ValidarToken,
							method: 'POST',
							data: {"accessToken":result.MensagemSucesso,
							"ClienteId": clienteCampanhaId 
						},
						}).then(function (r3) {
							localStorage.setItem('Usuario', JSON.stringify(r3.data));


							if (r3.data.PerfilNome.toLowerCase() == "adm cliente" ) {
								$window.location.href = decodeURIComponent($location.absUrl().split('?redirect=')[1] || url_site+'adm');
								return
							}
							else if (r3.data.PerfilNome.toLowerCase() == "participante" && !r3.data.PrimeiroAcesso) {
								$window.location.href = decodeURIComponent($location.absUrl().split('?redirect=')[1] || url_site+'cadastro');
								return
							}

							$window.location.href = decodeURIComponent($location.absUrl().split('?redirect=')[1] || url_site+'upload-de-nota-fiscal');
							return;

						})
					}

					else {
						$scope.errorLogin = true;
					}



				})



			}
			else {
				console.log('User cancelled login or did not fully authorize.');
			}
		},{ scope: 'email' });


	}

})