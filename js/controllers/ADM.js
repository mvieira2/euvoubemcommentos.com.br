meuApp.controller("ADM", function($scope, $rootScope, $timeout,$window, $http,$sce, API, Upload, Alertar){


	$scope.mesesFiltro = [{
		text : 'Novembro',
		mes : '11/2018'
	},{
		text : 'Dezembro',
		mes : '12/2018'
	},{
		text : 'Janeiro',
		mes : '01/2019'
	}]


	$scope.load = async function () {

		$scope.ganhadores = await API.post(WS.Participantes.ListarUsuarios, { usuarioId : JSON.parse(localStorage.getItem('Usuario'))['UsuarioId']});
		$scope.idsUsuariosUnics = []
		$scope.ganhadores = $scope.ganhadores.filter(ganhador => {return ganhador.DadosExtras });
		$scope.ganhadores.map(ganhador => {
			ganhador.DadosExtras = ganhador.DadosExtras && ganhador.DadosExtras != "Unholy Warcry" ? JSON.parse(ganhador.DadosExtras) : null;
		})

		$scope.notasGeradas = []

		$scope.ganhadores.map(ganhador => {

			if (ganhador.DadosExtras && angular.isString(ganhador.DadosExtras)) {
				ganhador.DadosExtras = JSON.parse(ganhador.DadosExtras)
			}

			if (angular.isObject(ganhador.DadosExtras)) {
				return ganhador.DadosExtras.notas.map(nota => {
					return {
						Nome : ganhador.Nome,
						PDVId : nota.pdvId,
						UsuarioId : ganhador.UsuarioId,
						nota : nota
					};
				})
			}

		}).filter(notas => notas != undefined).map(nota => {
			nota.map( nota1 => {
				$scope.notasGeradas.push( nota1 )
			})
		});

		$scope.notasGeradasCopy = $scope.notasGeradas;

		$scope.lista_atacadistas = await API.get( url_style + '/js/atacadistas.json');
		$scope.produtosDisplays = await API.get( url_style + '/js/produtos-participantes.json');

		$scope.$apply();
	};

	$scope.load().then(function () {
		$timeout(function () {
			// $scope.mesesFiltro[0].active = true
			$scope.filtrarMes('11/2018')
		},500)
	});

	$scope.isImage = function(url) {
		if (url) return(url.match(/\.(jpeg|jpg|gif|png)$/) != null);
	}

	$scope.addProduto = function (produtos) {
		produtos.push({
			item : null,
			Qtd: null
		})
	}

	$scope.removerProduto = function (produtos, index) {
		produtos.splice(index,1);
	}

	$scope.getQtdNumeroDaSorte = function () {
		// multiplos de 3 para gerar numero da sorte de todas as notas fiscais
		if ($scope.nota) {

			let result = $scope.arredondar($scope.nota.nota.produtos.reduce( (prevValor, notaNum) => { return prevValor + notaNum.Qtd }, 0) / 3);
			return result

		}


	}

	$scope.checkNumeroCupomExiste = function (num) {
		let cuponsExistentes = [];

		$scope.notasGeradas.map(nota => {

			if (nota.cuponsGerados && angular.isArray(nota.cuponsGerados)) {
				nota.cuponsGerados.map(cupom => {
					cuponsExistentes.push(cupom.numero);
				})
			}

		})

		return cuponsExistentes.indexOf(num) > -1;
	}

	$scope.getNumeroRandom = function () {
		let num = Math.floor((Math.random() * 99999) + 1).toString()

		if (num.length < 5) {
			for (var i = 0; i < (5 - num.length); i++) {
				num = '0' + num;
			}
		}



		// de 00.000 a 99.999
		return $scope.checkNumeroCupomExiste(num) ? $scope.getNumeroRandom() :  num;
	}


	$scope.gerarNumeroSerie = function (nota) {

		let serieRede = $scope.lista_atacadistas.filter(atacado => {
			return atacado.Nome.toLowerCase() == nota.PDVId.toLowerCase()
		})

		if (serieRede.length) {
			let numRede =  (serieRede[0]['serie_sorteio_mes_'+ moment( nota.nota.data, 'DD/MM/YYYY' ).format('MM') ] ||'' ); // serie conforme mes 
			let serie = numRede.toString().length == 1 ? '-0'+numRede.toString() : '-'+numRede.toString();
			return serie;
		}

		
		return '';
	}

	$scope.gerarNumeroDaSorte = function (nota) {

		if (!nota.nota.cuponsGerados || nota.nota.cuponsGerados && !angular.isArray(nota.nota.cuponsGerados)) {
			nota.nota.cuponsGerados = []
		}
		
		if ($scope.getQtdNumeroDaSorte() - nota.nota.cuponsGerados.length == 0 ||  $scope.getQtdNumeroDaSorte() < nota.nota.cuponsGerados.length) {
			Alertar.error('Todos números da sorte estão gerados ( conforme múltiplos de 3 displays), favor adicionar mais displays');
			$scope.salvarDadosExtras( $scope.ganhador.DadosExtras );
			return
		}

		let qtdNumSorte = $scope.getQtdNumeroDaSorte();
		let totalCupom = qtdNumSorte - nota.nota.cuponsGerados.length;

		if (confirm('Gerar os '+totalCupom+' cupons da sorte?')) {

			for (var i = 0; i < totalCupom; i++) {

				nota.nota.cuponsGerados.push({
					numero: $scope.getNumeroRandom() + $scope.gerarNumeroSerie(nota),
					data : moment().format('DD/MM/YYYY HH:MM:SS'),
					nota : nota.nota.numero
				})
			}



			$scope.salvarDadosExtras( $scope.ganhador.DadosExtras, function () {
				let cuponsGerados = $scope.nota.nota.cuponsGerados;

				let htmlEnvio = '';
				let htmlDefault = '<b style="font-size: 29px;">{numero}</b> <br>  da série: {serie} <br><br> Sorteio do dia {data} <br><br>';

				cuponsGerados.map(cupom => {
					htmlEnvio += htmlDefault.replace('{numero}', cupom.numero.split('-')[0] );
					htmlEnvio = htmlEnvio.replace('{serie}', cupom.numero.split('-')[1]);
					htmlEnvio = htmlEnvio.replace('{data}', $scope.getDataSorteio( moment($scope.nota.nota.data, 'DD/MM/YYYY') )  );  
				});

				let html = $('#aprovado').html();
				html = html.replace('{strings}', htmlEnvio);
				$scope.enviarEmail( $scope.nota.UsuarioId, 'Comprovantes aprovados', html );


			});
			return
		}




	}

	$scope.editarNota = function (nota) {

		$scope.nota = nota;

		$scope.nota.nota.campos_lock = angular.copy($scope.nota.nota.rejeitado )

		$scope.ganhador = $scope.ganhadores.filter(ganhador => ganhador.UsuarioId == $scope.nota.UsuarioId)[0] 

		if (!$scope.nota.nota.produtos) {

			$scope.nota.nota.produtos = [{
				item : null,
				Qtd: null
			}]

		}



	}

	$scope.apenasSalvar = function() {
		$scope.salvarDadosExtras( $scope.ganhador.DadosExtras );
	}

	$scope.enviarEmail = async function (usuarioDestinatarioIdM, assunto, corpo ) {

		await API.post(WS.Participantes.EnviarEmailGenerico, {
			usuarioId : JSON.parse(localStorage.getItem('Usuario'))['UsuarioId'],
			usuarioDestinatarioId : usuarioDestinatarioIdM,
			clienteCampanhaId : JSON.parse(localStorage.getItem('Usuario'))['ClienteCampanhaId'],
			assuntoEmail : assunto,
			corpoEmail : corpo,
		})

	}

	$scope.finalizar = function() {
		if (confirm('Informar o participante por email?')) {
			$scope.nota.nota.campos_lock = true;

			let html =  $('#naoaprovado').html();

			$scope.salvarDadosExtras( $scope.ganhador.DadosExtras, function () {
				$scope.enviarEmail( $scope.nota.UsuarioId, 'Comprovantes não foram aprovados', html )
			});
		}

		else {
			$scope.salvarDadosExtras( $scope.ganhador.DadosExtras);
		}
	}

	$scope.getDataSorteio = function (data) {
		// data sorteio
		// serie 01 a 35 > 01/11/2018 a 30/11/2018 > 08/12/2018
		// serie 36 a 70 > 01/12/2018 a 04/01/2019 > 12/01/2019
		// serie 71 a 105 > 05/01/2019 a 31/01/2019 > 09/02/2019
		// moment('2010-10-20').isBetween('2010-10-19', '2010-10-25'); // true

		if (moment(data, 'DD/MM/YYYY').isBetween('2018-10-31', '2018-12-01')) {
			return '08/12/2018';

		} else if (moment(data, 'DD/MM/YYYY').isBetween('2018-11-30', '2019-01-05')) {
			return '12/01/2019';
			
		} else if (moment(data, 'DD/MM/YYYY').isBetween('2019-01-04', '2019-02-01')) {
			return '09/02/2019';
		}

		return null;

	}


	$scope.salvarDadosExtras = async function (dados, callback) {

		let dadosExtrasCopy = JSON.stringify(dados);

		API.post(WS.Participantes.SalvarDadosExtras, {
			usuarioId : $scope.nota.UsuarioId,
			dadosExtras : dadosExtrasCopy

		}).then(function (data) {
			if (data.OperacaoRealizadaComSucesso) {
				$scope.load().then(function () {

					//$scope.filtrarMes();
					$scope.participante = {};
					$scope.endereco = {};
					Alertar.success('Dados salvos');

					if (typeof callback === 'function') {
						callback();
					}

					$rootScope.$apply();

				})
			}

			else if (data.OperacaoRealizadaComSucesso == false) {
				Alertar.error(data.MensagemErro);
				$rootScope.$apply();
			}


		})


	}

	$scope.editarStatus = function() {
		$scope.nota.nota.rejeitado = false
		$scope.nota.nota.justificativa = '';
		$scope.nota.nota.campos_lock = false;
	}

	$scope.arredondar = function(x) {
		return x < 0 ? Math.ceil(x) : Math.floor(x);
	}

	$scope.limparFiltroData = function (filtro) {
		if (!filtro.data) {
			delete filtro.data
		}

	}

	$scope.limparFiltroPDVid = function (filtro) {
		if (!filtro.PDVId) {
			delete filtro.PDVId
		}
	}

	$scope.filtrarMes = function (mesAno) {
		$scope.nota = null

		$scope.mesAno = mesAno;
		
	}

	$timeout(function () {
		$('.nav-tabs li').eq(0).addClass('active')
		$('.nav-tabs a, .bt_nav_menu').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
		})  
	},500)



}).filter('trustThisUrl', ["$sce", function ($sce) {
	return function (val) {
		return $sce.trustAsResourceUrl(val);
	};
}]).filter('mesAno', function () {
	return function(input, data1, param2) {

		var newValue;

		if (input) {
			
			newValue = input.filter(mes => {
				return moment(mes.nota.data, 'DD/MM/YYYY').format('MM/YYYY') == data1;
			})
		}
      // Funcionalidade do filtro
      return newValue;
  }
}).filter('noDuplicateData', function () {
	return function(input, data1, param2) {


		if (input && input.length>0) {
			var newValue = [];

			var dataOlds = []

			dataOlds = input.map(mes => {
				return mes.nota.data
			}).filter(function(item, pos, self) {

				return self.indexOf(item) == pos;
			})

			dataOlds.map((data) => {

				newValue.push( input.filter(mes => {
					return mes.nota.data == data;
				})[0])


			});

			return newValue;
		}
      // Funcionalidade do filtro
      return input;
  }
}).filter('noDuplicatePDV', function () {
	return function(input, data1, param2) {


		if (input && input.length>0) {
			var newValue = [];

			var dataOlds = []

			dataOlds = input.map(mes => {
				return mes.PDVId
			}).filter(function(item, pos, self) {

				return self.indexOf(item) == pos;
			})


			dataOlds.map((data) => {

				newValue.push( input.filter(mes => {
					return mes.PDVId == data;
				})[0])


			});

			return newValue;
		}
      // Funcionalidade do filtro
      return input;
  }
})
