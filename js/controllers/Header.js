meuApp.controller("Header", function($window, $scope, $rootScope, $location,$sce, $http, $timeout, API){

	if (!localStorage.getItem('Usuario')) {

		alert('Favor efetuar login...');
		$window.location.href = decodeURIComponent($location.absUrl().split('?redirect=')[1] || url_site);

		return
	}

	$scope.load = async function () {
		$rootScope.participante = await API.post( WS.Participantes.ObterDadosPremiado, { usuarioId : JSON.parse(localStorage.getItem('Usuario'))['UsuarioId'] } );
		$rootScope.participante =  $rootScope.participante || [];
		$rootScope.participante.Nascimento = moment($rootScope.participante.Nascimento).format('DD/MM/YYYY');

		$scope.usuario = {
			nome : $rootScope.participante.Nome || JSON.parse(localStorage.getItem('Usuario')).PremiadoNome,
		}




		$rootScope.$apply();
	}

	$scope.load();

	$scope.logout = function (argument) {

		if (confirm('Deseja realmente sair?')) {
			WS.Sair();
		}
		
	}
});