meuApp.controller("Extrato", function($scope, $rootScope, $timeout,$window, $http, API){

	$scope.mesesFiltro = [{
		text : 'Novembro',
		mes : '11/2018'
	},{
		text : 'Dezembro',
		mes : '12/2018'
	},{
		text : 'Janeiro',
		mes : '01/2019'
	}]


	$rootScope.$watch('participante', function(newValue, oldValue) {
		if (newValue) {
			$scope.dadosExtras = $rootScope.participante.DadosExtras != '' ?  JSON.parse($rootScope.participante.DadosExtras) : null;

			$scope.cuponsGerados = [];
			$scope.filtrarMes('11/2018');

			if ($scope.dadosExtras && $scope.dadosExtras.notas) {


				$scope.dadosExtras.notas.map(nota => {

					if (nota.cuponsGerados) {


						return nota.cuponsGerados.map(cupom => {

							$scope.cuponsGerados.push({
								nota: nota.numero,
								data: nota.data,
								cupom : cupom.numero,
								pdvId : nota.pdvId
							})



						})

					}
				})
			}
		}
		
	});
	$scope.limparFiltro = function (elem) {


		if (!$scope.filtro[elem]) {
			delete $scope.filtro[elem]
		}
	}

	$scope.filtrarMes = function (mesAno) {
		$scope.mesAno = mesAno;
		
	}



	$timeout(function () {
		$('.nav-tabs li').eq(0).addClass('active')
		$('.nav-tabs a, .bt_nav_menu').click(function (e) {
			e.preventDefault();
			$(this).tab('show');
		})  
	},500)


}).filter('mesAno', function () {
	return function(input, data1, param2) {

		var newValue;

		if (input) {
			
			newValue = input.filter(mes => {
				return moment(mes.data, 'DD/MM/YYYY').format('MM/YYYY') == data1;
			})
		}
      // Funcionalidade do filtro
      return newValue;
  }
}).filter('noDuplicatePDV', function () {
	return function(input, data1, param2) {


		if (input && input.length>0) {
			var newValue = [];

			var dataOlds = []

			dataOlds = input.map(mes => {
				return mes.pdvId
			}).filter(function(item, pos, self) {

				return self.indexOf(item) == pos;
			})


			dataOlds.map((data) => {

				newValue.push( input.filter(mes => {
					return mes.pdvId == data;
				})[0])


			});

			return newValue;
		}
      // Funcionalidade do filtro
      return input;
  }
})
