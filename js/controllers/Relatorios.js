meuApp.controller("Relatorios", function($scope, $rootScope, $timeout,$window, $http,$sce, API, Upload, Alertar){

	$scope.load = async function () {
		$scope.ganhadores = await API.post(WS.Participantes.ListarUsuarios, { usuarioId : JSON.parse(localStorage.getItem('Usuario'))['UsuarioId']});
		$scope.ganhadores = $scope.ganhadores.filter(ganhador => {return ganhador.DadosExtras != null })

		$scope.ganhadores.map(ganhador => {
			ganhador.DadosExtras = ganhador.DadosExtras && ganhador.DadosExtras != "Unholy Warcry" ? JSON.parse(ganhador.DadosExtras) : null;
		});

		$scope.relatorio1 = []; //Relatório de participante por rede
		$scope.ganhadores.map(ganhador => {

			if (ganhador.DadosExtras && angular.isArray(ganhador.DadosExtras.notas) ) {

				return ganhador.DadosExtras.notas.map(nota => {
					return {
						nome : ganhador.Nome,
						cpf: ganhador.CPF_CNPJ,
						rede : nota.pdvId,
						nota_numero: nota.numero,
						nota_data : nota.data,
						qtd_produtos_comprados : angular.isArray(nota.produtos) ? nota.produtos.reduce((prevVal, prod) => {
							return prevVal + prod.Qtd;
						}, 0) : 0,
					}
				})

			}
		}).filter(arr => arr != null && arr.length ).map(arr => {
			arr.map(arr2 => {
				$scope.relatorio1.push(arr2);
			})
		});

		
		$scope.relatorio2 = []; //Relatório de Cupons por Rede

		$scope.ganhadores.map(ganhador => {

			if (ganhador.DadosExtras && angular.isArray(ganhador.DadosExtras.notas) ) {

				return ganhador.DadosExtras.notas.map(nota => {
					if (angular.isArray( nota.cuponsGerados)) {

						return nota.cuponsGerados.map(cupom => {
							return {
								cupom_numero : cupom.numero,
								cupom_rede : nota.pdvId,
								cupom_data : moment(cupom.data, 'DD/MM/YYYY HH:MM').format('DD/MM/YYYY'),
								nota_num : cupom.nota,
								nota_data : nota.data,
								nome : ganhador.Nome,
								cpf: ganhador.CPF_CNPJ,
							}
						})
					}
				}).filter(arr => arr != null && arr.length )

			}
		}).filter(arr => arr != null && arr.length ).map(arr => {
			arr.map(arr2 => {
				arr2.map(arr3 => {
					$scope.relatorio2.push(arr3);
				})
			})
		})

		$scope.$apply();

	}

	$scope.load();


});