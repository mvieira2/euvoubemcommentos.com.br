meuApp.controller("Cadastro", function($scope, $rootScope, $timeout,$window, $http, API, Alertar, Upload){


	$scope.getImageBase64 = function (arquivos) {
		File.prototype.convertToBase64 = function(callback){
			var reader = new FileReader();
			reader.onloadend = function (e) {
				callback(e.target.result, e.target.error);
			};   
			reader.readAsDataURL(this);
		};

		angular.forEach(arquivos[0].files, function(files, key) {
			console.log(files)
			files.convertToBase64(function(base64){	  
				$scope.$apply(function () {
					$scope.img = base64;
				});

			})	
		});


	}

	$scope.getCidades = async function (UF) {
		if (UF) {

			$scope.cidades = await API.post( WS.Participantes.ObterEnderecoCidadesComDistinct, UF );
			$scope.cidades =  $scope.cidades || [];

			$scope.$apply();
		}
	}

	$scope.load = async function() {

		$scope.usuario = JSON.parse(localStorage.getItem('Usuario'));


		$scope.esdadosCivis = await API.post( WS.Participantes.EstadoCivil );
		$scope.esdadosCivis =  $scope.esdadosCivis || [];

		$scope.sexos = await API.post( WS.Participantes.Sexo );
		$scope.sexos =  $scope.sexos || [];

		$scope.sexos.push({
			Id : 3 ,
			Descricao : 'Não quero informar' ,
		});


		$scope.tiposTelefone = await API.post( WS.Participantes.TiposTelefone );
		$scope.tiposTelefone =  $scope.tiposTelefone || [];

		$scope.estados = await API.post( WS.Participantes.EnderecoEstados );
		$scope.estados =  $scope.estados || [];
		
		$scope.tipoEnderecos = await API.post( WS.Participantes.TiposEndereco );
		$scope.tipoEnderecos =  $scope.tipoEnderecos || [];		

		$scope.tipoLogradouros = await API.post( WS.Participantes.TiposLogradouro );
		$scope.tipoLogradouros =  $scope.tipoLogradouros || [];


		$rootScope.participante = await API.post( WS.Participantes.ObterDadosPremiado, { usuarioId : JSON.parse(localStorage.getItem('Usuario'))['UsuarioId'] } );
		$rootScope.participante =  $rootScope.participante || [];
		$rootScope.participante.Nascimento = moment($rootScope.participante.Nascimento).format('DD/MM/YYYY');

		$rootScope.participante.name = $rootScope.participante.Nome.split(' ')[0];
		$rootScope.participante.sobrenome = $rootScope.participante.Nome.replace($rootScope.participante.name ,'');


		if ($rootScope.participante.PrimeiroAcesso) {
			Alertar.warning('Você precisa completar o seu cadastro');
		}



		Utils.Masks();
		$scope.$apply();
		$rootScope.$apply();
	}


	$scope.salvarDadosPrincipais = async function () {
		
		
		let modelParticipante = {
			usuarioId: $scope.usuario.UsuarioId,
			premiadoId: $rootScope.participante.PremiadoId,
			senhaAtual: $rootScope.participante.senhaAtual,
			novaSenha: $rootScope.participante.novaSenha,
			email: $rootScope.participante.Email,
			estadoCivilId: $rootScope.participante.EstadoCivilId,
			sexoId: $rootScope.participante.SexoId,
			CPF_CNPJ: $rootScope.participante.CPF_CNPJ,
			nascimento: '/Date('+moment( $rootScope.participante.Nascimento, 'DD/MM/YYYY').valueOf()+')/',
			Nome : $rootScope.participante.name +' '+$rootScope.participante.sobrenome 
		};


		if ($scope.file) {
			let formData = new FormData();
			let name = $rootScope.participante.PremiadoId +"."+$scope.file.type.split('/')[1].toString();

			formData.append('file', $('#file')[0].files[0], name);				


			$.ajax({
				url: WS.Participantes.EnviarArquivo,
				type: "POST",
				data: formData,
				contentType: false,
				processData: false,
				success: function (data) {

					let salvarImg = {
						usuarioId: $scope.usuario.UsuarioId,
						premiadoId: $rootScope.participante.PremiadoId,
						arquivoNome : data.Url
					};

					API.post( WS.Participantes.SalvarFoto, salvarImg).then(function (data1) {
						if (data1.OperacaoRealizadaComSucesso) {
							Alertar.success(data1.MensagemSucesso);
						}

						else if (data1.OperacaoRealizadaComSucesso == false) {
							Alertar.error(data1.MensagemErro);
						}

					});


				}
			});


		};


		// Dados do participante
		let updateDadosParticipante = await API.post(WS.Participantes.SalvarDados, modelParticipante);
		if (updateDadosParticipante.OperacaoRealizadaComSucesso) {
			Alertar.success('Dados pessoais foram salvo com sucesso.' );
		}

		else if (updateDadosParticipante.OperacaoRealizadaComSucesso == false) {
			Alertar.error(updateDadosParticipante.MensagemErro);
		}



		$scope.load().then(function () {
			
			Utils.Masks();
		});

	}


	$scope.salvarSenha = async function () {

		let modelParticipante = {
			usuarioId: $scope.usuario.UsuarioId,
			premiadoId: $rootScope.participante.PremiadoId,
			senhaAtual: $scope.senha.senhaAtual,
			novaSenha: $scope.senha.novaSenha,
		};


		// alterando a senha
		if (modelParticipante.senhaAtual != null && modelParticipante.novaSenha != null) {
			let senhaAlterada = await API.post(WS.Autenticar.AlterarSenha, modelParticipante);

			if (senhaAlterada.OperacaoRealizadaComSucesso) {
				Alertar.success('Senha alterada com sucesso!');
			}

			else if (senhaAlterada.OperacaoRealizadaComSucesso == false) {
				Alertar.error(senhaAlterada.MensagemErro);
			}


		}



		$scope.load();

		Utils.Masks();
	}

	$scope.salvarEnderecos = async function (end, isValid) {

		if (!isValid) {
			Alertar.error('Favor preencher todos os campos obrigatórios');
			$rootScope.$apply();
			return
		}

		if (end) {

			let endereco = angular.copy(end);

			let modelEndereco = {
				Bairro: endereco.Bairro,
				CEP: endereco.CEP.replace('-',''),
				CidadeId: endereco.CidadeId,
				Complemento: endereco.Complemento,
				Logradouro: endereco.Logradouro,
				Numero: endereco.Numero,
				PremiadoEnderecoId: endereco.PremiadoEnderecoId || null,
				TipoEnderecoId: endereco.TipoEnderecoId,
				TipoLogradouro: endereco.TipoLogradouro,
				UF: endereco.UF,
				UsuarioId : $scope.usuario.UsuarioId,
				PremiadoId : $rootScope.participante.PremiadoId,
				FlagEnderecoPrincipal : Boolean(endereco.FlagEnderecoPrincipal)
			}


			for(var k in modelEndereco){
				if (modelEndereco[k] == null) {
					delete modelEndereco[k];
				}
			}


			let result = await API.post(WS.Participantes.SalvarEndereco, modelEndereco);


			if (result.OperacaoRealizadaComSucesso) {
				$scope.load();
				Alertar.success('Endereço foi salvo com sucesso.' );
			}

			else if (result.OperacaoRealizadaComSucesso == false) {
				Alertar.error(result.MensagemErro);
			}




			Utils.Masks();
		}
	}

	$scope.salvarTelefone = async function (telefone) {
		let modelTelefone = angular.copy(telefone);

		modelTelefone.UsuarioId = $scope.usuario.UsuarioId;
		modelTelefone.PremiadoId = $rootScope.participante.PremiadoId;
		modelTelefone.FlagTelPrincipal = Boolean(modelTelefone.FlagTelPrincipal);

		let result = await API.post(WS.Participantes.SalvarTelefone, modelTelefone);

		if (result.OperacaoRealizadaComSucesso) {
			Alertar.success(result.MensagemSucesso);
			telefone = {};
			$scope.load();
		}

		else if (result.OperacaoRealizadaComSucesso == false) {
			Alertar.error(result.MensagemErro);
		}


		$scope.$apply();			

	}

	$scope.excluirTelefone = async function (telefone) {

		if (confirm('Deseja realmente exluir este telefone')) {

			let result = await API.post(WS.Participantes.ExcluirTelefone, telefone);

			if (result.OperacaoRealizadaComSucesso) {
				Alertar.success(result.MensagemSucesso);
				$scope.load();
			}

			else if (result.OperacaoRealizadaComSucesso == false) {
				Alertar.error(result.MensagemErro);
			}
		}

	}

	$scope.editarTel = function (tel) {
		$scope.telefone = [];
		$scope.telefone = angular.copy(tel);
	}

	$scope.editarEndereco = function (end) {

		$scope.endereco = {};

		$scope.getCidades(end.UF).then(function () {

			$scope.endereco = angular.copy(end);

			let cidadeId = $scope.cidades.filter(cidade => cidade.Nome == end.Cidade)[0];
			$scope.endereco.CidadeId =  cidadeId.Constante || '';
			$scope.endereco.Numero =  Number($scope.endereco.Numero) || '';

			$scope.$apply();
		});



	}

	$scope.excluirEndereco = async function (endereco) {

		if (confirm('Deseja realmente exluir este endereço')) {

			let result = await API.post(WS.Participantes.ExcluirEndereco, endereco);

			if (result.OperacaoRealizadaComSucesso) {
				Alertar.success(result.MensagemSucesso);
				$scope.load();
			}

			else if (result.OperacaoRealizadaComSucesso == false) {
				Alertar.error(result.MensagemErro);
			}
		}

	}



	$scope.buscarCEP = function (cep) {

		if ( cep.toString().length == 9 ) {
			$http({
				url: 'https://viacep.com.br/ws/'+ cep.toString().replace('-','') +'/json/',
				method: "GET",
			}).then(function mySuccess(r) {
				$scope.cepTeste = r.data
				$scope.endereco.UF = r.data.uf.toUpperCase();
				$scope.endereco.Logradouro = r.data.logradouro;
				$scope.endereco.Bairro = r.data.bairro;

				$scope.getCidades($scope.endereco.UF).then(function () {

					let cidadeId = $scope.cidades.filter(cidade => cidade.Nome == r.data.localidade.toUpperCase())[0]

					$scope.endereco.CidadeId = cidadeId.Constante || '';

					$scope.$apply();
					
					
				});
				
			});
		}
	}



	$scope.load();

	$('.nav-tabs a, .bt_nav_menu').click(function (e) {
		e.preventDefault();
		$(this).tab('show');
	})

	$timeout(function() {
		Utils.Masks();
	},500)

});