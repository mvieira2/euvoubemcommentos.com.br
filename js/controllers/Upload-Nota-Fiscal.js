meuApp.controller("Upload-Nota-Fiscal", function($scope, $rootScope, $window, $timeout, $http, API, Upload, Alertar){


	var bucketName = 'files.hsolmkt.com.br';
	AWS.config.region = 'sa-east-1';

	AWS.config.update({
		accessKeyId: "AKIAJBNKFJK3EETXP77Q",
		secretAccessKey: "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",
	});

	var bucket = new AWS.S3();

	$scope.load = async function () {

		$rootScope.lista_atacadistas = await API.get( url_style + '/js/atacadistas.json');

		$rootScope.participante = await API.post( WS.Participantes.ObterDadosPremiado, { usuarioId : JSON.parse(localStorage.getItem('Usuario'))['UsuarioId'] } );
		$rootScope.participante =  $rootScope.participante || [];
		$rootScope.participante.Nascimento = moment($rootScope.participante.Nascimento).format('DD/MM/YYYY');

		$scope.participante.DadosExtras = $scope.participante.DadosExtras ? JSON.parse($scope.participante.DadosExtras) : {};

		if (angular.isObject($scope.participante.DadosExtras) && !$scope.participante.DadosExtras.lojas) {
			$scope.participante.DadosExtras = {};
		}

		if (angular.isString($scope.participante.DadosExtras)) {
			$scope.participante.DadosExtras = JSON.parse($scope.participante.DadosExtras)
		}



		$rootScope.$apply();
	}

	$scope.load();


	$scope.subirNotaFiscal = function(file) {

		$scope.sucesso = null;

		let idCampanha = JSON.parse(localStorage.getItem('Usuario'))['ClienteCampanhaId'];
		let idClienteCartao = JSON.parse(localStorage.getItem('Usuario'))['ClienteCartaoCampanhaId'];

		if (idCampanha && idClienteCartao) {

			var path_file = 'eu_vou_bem_com_mentos_e_fruittella/cupons/'+idCampanha+'/'+ idClienteCartao +'/'+moment().unix()+'-' + file.name

			var params = {
				Bucket: bucketName,
				Key: path_file,
				ContentType: file.type,
				Body: file,
				ACL: 'public-read'
			};

			bucket.putObject(params, function (err, data1) {
				if (err) {
					$scope.sucesso = false;
					Alertar.error(err);
					return false;
				} else {
					$scope.link = 'https://s3.sa-east-1.amazonaws.com/'+bucketName + '/'+path_file;

					let dadosExtrasCopy =  $scope.participante.DadosExtras;

					if (dadosExtrasCopy.notas && angular.isArray(dadosExtrasCopy.notas)) {

						dadosExtrasCopy.notas.push({
							data: $scope.cupom.data,
							nota_link: $scope.link,
							numero:  $scope.cupom.numero,
							pdvId: $scope.cupom.pdvId ,
							idCampanha : idCampanha,
							idClienteCartao: idClienteCartao

						});
					}

					else {


						dadosExtrasCopy.notas = [{
							data:$scope.cupom.data,
							nota_link: $scope.link,
							numero:  $scope.cupom.numero,
							pdvId: $scope.cupom.pdvId ,
							idCampanha : idCampanha,
							idClienteCartao: idClienteCartao
						}];
					}

					API.post(WS.Participantes.SalvarDadosExtras, {
						usuarioId : JSON.parse(localStorage.getItem('Usuario'))['UsuarioId'],
						dadosExtras : JSON.stringify(dadosExtrasCopy)

					}).then(function (data) {
						if (data.OperacaoRealizadaComSucesso) {
							$scope.load().then(function () {
								delete $scope.cupom;
								//delete $scope.file;

								$('#modalOk').modal();

								$scope.sucesso = true;
								Alertar.success('Nota fiscal enviada, fique atento aos seus email que após validação lhe enviaremos o número da sorte');
								$rootScope.$apply();
								

								
							})
						}

						else if (data.OperacaoRealizadaComSucesso == false) {
							Alertar.error(data.MensagemErro);
							$rootScope.$apply();
						}


					})



				}

			})

		}


	};

	$timeout(function() {
		Utils.Masks();

		$('.date').datepicker({
			language: "pt-BR",
			autoclose: true,
			startDate: new Date(2018, 11 - 1, '01'),
			endDate: new Date(2019, 0, '31')
		});


	},500)

}).filter('mesAno', function () {
	return function(input, data1, param2) {

		var newValue;

		if (input) {
			
			newValue = input.filter(mes => {
				return moment(mes.nota.data, 'DD/MM/YYYY').format('MM/YYYY') == data1;
			})
		}
      // Funcionalidade do filtro
      return newValue;
  }
})
