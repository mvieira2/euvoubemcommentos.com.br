meuApp.controller("Participar", function($scope, $rootScope, $location, $timeout,$window, $http, API, Upload, Alertar){

	var bucketName = 'files.hsolmkt.com.br';
	AWS.config.region = 'sa-east-1';

	AWS.config.update({
		accessKeyId: "AKIAJBNKFJK3EETXP77Q",
		secretAccessKey: "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",
	});

	var bucket = new AWS.S3();


	$scope.load = async function () {
		$scope.sexos = await API.post( WS.Participantes.Sexo );
		$scope.sexos =  $scope.sexos || [];

		$scope.estados = await API.post( WS.Participantes.EnderecoEstados );
		$scope.estados =  $scope.estados || [];

		$scope.campanha = await API.post(WS.Configuracoes.Url, { url : 'http://www.euvoubemcommentos.com.br' });
		$scope.campanha = $scope.campanha.ObterConfiguracaoUrlResult || []; 

		if (Boolean(sessionStorage.getItem('facebookUser'))) {
			$scope.alertaFacebook = true;
			const userFacebook = JSON.parse(sessionStorage.getItem('facebookUser'));

			$scope.participante = {
				name : userFacebook.first_name,
				sobrenome : userFacebook.last_name,
				email1 : userFacebook.email,
				dtNasc : userFacebook.birthday,

			}
		}

		$scope.$apply();

	}

	$scope.load();

	$scope.getDadosFacebook = function () {
		FB.api('/me', function(response) {
			debugger

		});
	}



	$scope.getCidades = async function (UF) {
		if (UF) {

			$scope.cidades = await API.post( WS.Participantes.ObterEnderecoCidadesComDistinct, UF );
			$scope.cidades =  $scope.cidades || [];

			$scope.$apply();
		}
	}


	$scope.subirNotaFiscal = async function(file, idUsuario) {

		$scope.sucesso = null;

		let idCampanha = $scope.campanha['ClienteCampanhaId'];
		let idClienteCartao = idUsuario;

		if (idCampanha && idClienteCartao) {

			var path_file = 'eu_vou_bem_com_mentos_e_fruittella/cupons/'+idCampanha+'/'+ idClienteCartao +'/'+moment().unix()+'-' + file.name

			var params = {
				Bucket: bucketName,
				Key: path_file,
				ContentType: file.type,
				Body: file,
				ACL: 'public-read'
			};

			bucket.putObject(params, function (err, data1) {
				if (err) {
					$scope.sucesso = false;
					Alertar.error(err);
					return false;
				} else {
					$scope.sucesso = true;

					$scope.link = 'https://s3.sa-east-1.amazonaws.com/'+bucketName + '/'+path_file;

				}
			})

		}


	};

	$scope.cadastrar = async function(file) {

		$scope.sucesso = null;

		let modelCadastro = Object.assign({}, angular.copy( $scope.participante ), angular.copy( $scope.endereco ) );
		modelCadastro.campanhaId = $scope.campanha['ClienteCampanhaId'];
		modelCadastro.cep = modelCadastro.cep.replace(/\D/g, '');
		modelCadastro.nome = modelCadastro.name + ' ' + modelCadastro.sobrenome;

		modelCadastro.telefone = modelCadastro.DDD2 + ' ' + modelCadastro.telefone;

		let result = await API.post(WS.Autenticar.CadastrarGigantes, modelCadastro);

		if (result.OperacaoRealizadaComSucesso) {

			if (file) {


				$scope.subirNotaFiscal(file, result.Id).then(function () {

					API.post(WS.Participantes.SalvarDadosExtras, {
						usuarioId : result.Id,
						dadosExtras : JSON.stringify([{
							data: moment().format('DD/MM/YYYY'),
							nota_link: $scope.link,
							idCampanha : idCampanha,
							idClienteCartao: idClienteCartao
						}])

					}).then(function (data) {
						if (data.OperacaoRealizadaComSucesso) {
							sessionStorage.removeItem("facebookUser");
							Alertar.success('Usuário cadastrado com sucesso, aguarde efetuando seu login');

							$rootScope.logar({login : $scope.participante.email, senha : $scope.participante.senha, url:Portal_Url  }, true);
							$scope.endereco = {};
							$scope.participante = {};

						}

						else if (data.OperacaoRealizadaComSucesso == false) {
							Alertar.error(data.MensagemErro);
						}

						$rootScope.$apply();

					})





				});

			}

			else {
				if (result.OperacaoRealizadaComSucesso) {
					Alertar.success('Usuário cadastrado com sucesso, aguarde efetuando seu login');

					$rootScope.logar({login : $scope.participante.email, senha : $scope.participante.senha, url:Portal_Url.replace('br/','br')  }, true);
					$scope.endereco = {};
					$scope.participante = {};


					$rootScope.$apply();
				}


			}

		}

		else if (result.OperacaoRealizadaComSucesso == false) {
			Alertar.error(result.MensagemErro);
			$rootScope.$apply();
		}





	};

	$scope.showModalRegulamento = function (argument) {
		$('#regulamento').modal()
	}

	$scope.buscarCEP = function (cep) {

		if ( cep.toString().length == 9 ) {
			$http({
				url: 'https://viacep.com.br/ws/'+ cep.toString().replace('-','') +'/json/',
				method: "GET",
			}).then(function mySuccess(r) {
				$scope.cepTeste = r.data
				$scope.endereco.estado = r.data.uf.toUpperCase();
				$scope.endereco.endereco = r.data.logradouro;
				$scope.endereco.bairro = r.data.bairro;

				$scope.getCidades($scope.endereco.estado).then(function () {

					let cidade = $scope.cidades.filter(cidade => cidade.Nome == r.data.localidade.toUpperCase())[0]

					$scope.endereco.cidade = cidade.Nome || '';

					$scope.$apply();
					
					
				});
				
			});
		}
	}


	$timeout(function() {
		Utils.Masks();

		$('.date').datepicker({
			language: "pt-BR",
			autoclose: true,
		});

	},500)

})
