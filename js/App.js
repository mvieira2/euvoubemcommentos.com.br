var meuApp = angular.module("meuApp", ['ngFileUpload', 'ngSanitize'] );

var autoricacaoToken = localStorage.getItem('oAuth');


meuApp.config(function ($httpProvider) {
	$httpProvider.interceptors.push(function ($q, $rootScope) {
		return {
			'request': function(config) {
				Utils.IconeLoaderOn();
				return config;
			},
			'requestError': function(rejection) {
				Utils.IconeLoadeOff();
				return $q.reject(rejection);
			},
			'response': function(response) {
				Utils.IconeLoadeOff();
				return response;
			},
			'responseError': function(rejection) {
				Utils.IconeLoadeOff();
				return $q.reject(rejection);
			}
		};

	});
}).factory("API", function($http, $rootScope){

	var api = {
		alertar : function (msg) {
			$.notify({
				message: msg,
				icon: 'fa fa-check-circle',
			},{
				type:'success',
				icon: 'glyphicon glyphicon-star',
				z_index: 9999999999,
				//delay: 1000,
				// animate: {
				// 	enter: 'animated zoomInDown',
				// 	exit: 'animated zoomOutUp'
				// },
				placement: {
					from: "top",
					align: "right"
				}
			});
		},
		alertar_erro :  function (msg) {
			$.notify({
				message: msg,
				//icon: 'glyphicon glyphicon-alert',
			},{
				type:'danger', 
				z_index: 9999999999,
				//delay: 2000,
				// animate: {
				// 	enter: 'animated zoomInDown',
				// 	exit: 'animated zoomOutUp'
				// },
				placement: {
					from: "top",
					align: "right"
				}});
		},
		call : function (chamada, metodo,parametros, alertarErro ) {
			return $http({
				url: chamada,
				method: (metodo||'POST'),
				data: JSON.stringify(parametros||null),
				dataType: 'json',
				headers: {
					//"Authorization": autoricacaoToken,
					'Content-Type': 'application/json'
				},
			}).then(function (r) {

				return r.data;

			}, function (error) {
				
				return  error.data;
			});
		},

	}

	return {
		solicitar : api.call,
		get : function(chamada, opts, alertarErro){
			return api.call(chamada, 'GET', opts, alertarErro);
		},
		post : function(chamada,opts, alertarErro){
			return api.call(chamada, 'POST', opts, alertarErro);
		},
		get_params_url: function (param, url) {
			var url = new URL(url||window.location.href);
			return url.searchParams.get(param);
		},
		b64EncodeUnicode : function (str) {
			return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,
				function toSolidBytes(match, p1) {
					return String.fromCharCode('0x' + p1);
				})).replace('==','');
		},
		b64DecodeUnicode : function (str) {
			return decodeURIComponent(atob(str).split('').map(function(c) {
				return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
			}).join(''));
		},		
		alertar: api.alertar,
		alertar_erro: api.alertar_erro
	};

}).directive('file', function () {
	return {
		scope: {
			file: '='
		},
		link: function (scope, el, attrs) {
			el.bind('change', function (event) {
				var file = event.target.files[0];
				scope.file = file ? file : undefined;
				scope.$apply();
			});
		}
	};
}).filter('formatarPontos', function() {
	return function(input) {

		if (input) {
			input = input.toString() || '';

			while(input.includes(','))
			{
				input = input.replace(',', '.');
			}

			return input;
		}

		return input;
	};
}).directive('numericOnly', function(){
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, modelCtrl) {

			modelCtrl.$parsers.push(function (inputValue) {
				var transformedInput = inputValue ? inputValue.replace(/[^\d.-]/g,'') : null;

				if (transformedInput!=inputValue) {
					modelCtrl.$setViewValue(transformedInput);
					modelCtrl.$render();
				}

				return transformedInput;
			});
		}
	};
}).directive("alertas", function($rootScope) {
	return {
		restrict : "C",
		template : '<p class="animated fadeIn alert alert-{{alerta.tipo}}" ng-repeat="(key, alerta) in alertas track by $index"><a href="#" class="close" data-dismiss="alert" aria-label="close" ng-click="alertas.splice(key, 1)">&times;</a><strong>Aviso! </strong>  {{ alerta.message }} </p>',
	}

}).factory("Alertar", function($rootScope){
	$rootScope.alertas = [];

	var actions = {
		existAlert : function (tipo) {
			return $rootScope.alertas.filter(arr => arr.tipo === tipo);
		},
		updateAlert : function (tipo, msg) {
			let alerts = this.existAlert(tipo);
			if (alerts.length) {
				alerts.map(arr => arr.message = msg );
				return
			}
			$rootScope.alertas.push({tipo :tipo, message: msg});
		}
	};

	return {
		success : function (msg) {
			actions.updateAlert('success', msg);
		},
		warning : function (msg) {
			actions.updateAlert('warning', msg);
			//$rootScope.$apply();
		},
		error : function (msg) {
			actions.updateAlert('danger', msg);
			//$rootScope.$apply();
		},
	}
})