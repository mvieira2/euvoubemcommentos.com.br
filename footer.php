
<footer ng-controller="Footer" class="footer">
	<div class="comeca-menu ">
		<img class="mao-esquerda" src="<?php echo get_template_directory_uri()?>/img/mao-esquerda.png">
		<ul class="list-inline fundo-4">
			<li class="<?php echo is_page('regulamento') ? 'active' : ''; ?>">
				<a class="pointer" data-toggle="modal" data-target="#oregulamento">REGULAMENTO</a>
			</li>
			<li class="borderLeftBranco borderRightBranco <?php echo is_page('perguntas-frequentes') ? 'active' : ''; ?>">
				<a class="pointer" data-toggle="modal" data-target="#perguntas-frequentes">PERGUNTAS FREQUENTES</a>
			</li>
			<li class="borderRightBranco <?php echo is_page('fale-conosco') ? 'active' : ''; ?>">
				<a class="pointer" data-toggle="modal" data-target="#fale-conosco">FALE CONOSCO</a>
			</li>
		</ul>
		<img class="mao-direita" src="<?php echo get_template_directory_uri()?>/img/mao-direita.png">
	</div>
	<div class="center-2 redes-sociais">
		<div class="row">
			<div class="col-xs-6">
				<a target="_blank" href="https://www.facebook.com/mentosbr/">
					<i class="fa fa-facebook-square">
					</i>
				</a>
				<a target="_blank" href="https://www.instagram.com/mentosbrasil/?hl=pt" class="icon-instagram">
					<i class="fa fa-instagram">
					</i> <span class="jackerton">MENTOS</span>
				</a>
			</div>
			<div class="col-xs-6 desktop text-right">
				<a target="_blank" href="https://www.facebook.com/fruittellabrasil/" class="icon-facebook">
					<span class="jackerton">FRUIT-TELLA</span> <i class="fa fa-facebook-square"></i>
				</a>
				<a target="_blank" href="https://www.instagram.com/fruittellabrasil/?hl=pt-br" class="icon-instagram">
					<i class="fa fa-instagram"></i>
				</a>
			</div>
			<div class="col-xs-6 mobile">
				<a target="_blank" href="https://www.facebook.com/fruittellabrasil/" class="icon-facebook">
					<i class="fa fa-facebook-square"></i>
				</a>
				<a target="_blank" href="https://www.instagram.com/fruittellabrasil/?hl=pt-br" class="icon-instagram">
					<i class="fa fa-instagram"></i> <span class="jackerton">FRUIT-TELLA</span> 
				</a>
			</div>
		</div>

	</div>
	<div class="center-2 txtregulamento">
		<p class="text-center">
			Consulte o Regulamento completo e o Certificado de Autorização CAIXA no site www.euvoubemcommentos.com.br.
			<br>
			Período de Participação: 01 de novembro de 2018 a 31 de janeiro de 2019. <br>
			Certificado de Autorização CAIXA nº 4-7612/2018. Imagens meramente ilustrativas. <br> GUARDE TODOS OS COMPROVANTES FISCAIS CADASTRADOS
		</p> 
	</div>

	<!-- Modal Perguntas Frequentes -->
	<div id="perguntas-frequentes" class="modal fade  modal-estilo-1" role="dialog">
		<div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title ">PERGUNTAS FREQUENTES</h4>

				</div>
				<div class="modal-body ">
					<div class="corpo scroll">
						<?php
						$footer_page1 = get_page($id = get_page_by_path('perguntas-frequentes')->ID);
						echo apply_filters('the_content', $footer_page1->post_content);
						?>
					</div>
				</div>

			</div>

		</div>
	</div>

	<!-- Modal Regulamento-->
	<div id="oregulamento" class="modal fade  modal-estilo-1" role="dialog">
		<div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title ">REGULAMENTO</h4>
				</div>
				<div class="modal-body ">
					

					<div class="corpo scroll mobile">
						<?php
							$footer_page2 = get_page($id = get_page_by_path('regulamento')->ID);
							echo apply_filters('the_content', $footer_page2->post_content);
						?>
					</div>

					<div class="corpo scroll desktop">
						
						<div ng-show="regulamentoIsStatic" >
							<?php
							$footer_page2 = get_page($id = get_page_by_path('regulamento')->ID);
							echo apply_filters('the_content', $footer_page2->post_content);
							?>
						</div>
						<div ng-bind-html="regulamentoText"></div>
					</div>
					<button class="btn btn-success botao-5 " ng-click="aceitarRegulamento()" ng-show="botaoAceite">Aceitar regulamento</button>

					

				</div>

			</div>

		</div>
	</div>

	<!-- Modal Fale conosco -->
	<div id="fale-conosco" class="modal fade  modal-estilo-2" role="dialog">
		<div class="modal-dialog modal-lg">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title ">FALE CONOSCO</h4>
				</div>
				<div class="modal-body scroll">
					<?php
					$footer_page3 = get_page($id = get_page_by_path('fale-conosco')->ID);
					echo apply_filters('the_content', $footer_page3->post_content);
					?>

				</div>

			</div>

		</div>
	</div>

</footer>



<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/controllers/Footer.js?v2"></script>

<script type="text/javascript" src="<?php echo get_template_directory_uri()?>/js/script.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-118984422-15"></script>
<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-118984422-15');
</script>

<?php wp_footer(); ?>

</body>

</html>